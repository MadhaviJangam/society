<?php

  include 'include/admin-functions.php';
  include_once 'include/CSRF.class.php';


	$admin = new AdminFunctions();
	if($admin->sessionExists()){
		header("location: dashboard.php");
		exit();
	}

	if(isset($_POST['signin'])){
     
		//if($csrf->check_valid('post')) {
			$admin->adminLogin($_POST, "dashboard.php");
		//}
	}
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?php echo TITLE; ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/jquery.dataTables.min.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/validate.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<style>
.nav-link{
    color:black;font-size:15px; 
    width:100%;
}
.material-icons{
    display: inline-flex;
    vertical-align: top;
  
}
.active{
    border-bottom:2px solid #4834d4;
}
@media (min-width: 992px){
	.dropdown-menu .dropdown-toggle:after{
		border-top: .3em solid transparent;
	    border-right: 0;
	    border-bottom: .3em solid transparent;
	    border-left: .3em solid;
	}
	.dropdown-menu .dropdown-menu{
		margin-left:0; margin-right: 0;
	}
	.dropdown-menu li{
		position: relative;
	}
	.nav-item .submenu{ 
		display: none;
		position: absolute;
		left:100%; top:-7px;
	}
	.nav-item .submenu-left{ 
		right:100%; left:auto;
	}
	.dropdown-menu > li:hover{ background-color: #f1f1f1 }
	.dropdown-menu > li:hover > .submenu{
		display: block;
	}
}
 


/*
  Set the color of the icon
*/
svg path,
svg rect{
  fill: #FF6700;
}

.card{
border-top:4px solid #8e44ad;
border-radius:0px;
}

.card-header,.card-footer,.card-body {
    padding: 0.4rem 0.4rem;
    background:#fff;
    }
    .contentsection{
        margin-top:1%;
        margin-bottom:1%;
    }
.card-body > .form-group > label{
    font-size:10px;
    margin:1px;
}
.form-group > label{
    font-size:10px;
    margin:1px;
}
.form-group{
    margin:2px;
}
.form-control-sm {
    height: calc(1.5em + .5rem + 2px);
    padding: .2rem .2rem;
    border-radius: .0rem;
    border-bottom:1px solid #7f8c8d;
}


</style>

<body style="background:#f4f6fa;font-family: 'Noto Sans JP', sans-serif;">
<nav class="navbar navbar-expand-md navbar-light d-print-none navbardesktop" style="border-bottom:2px solid#eee;background:#FFF;padding:2px;">
  <a class="navbar-brand" href="#" style="color:black;font-size:14px;"><i class="material-icons">location_city</i> UNIQUE SOCIETY</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
</nav>

<br>
<br>

<div class="container-fluid">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-md-6">
       
                <img src="building.png" width="10%">
                <img src="building.png" width="10%" style="float:right;">
            <div class="card">
   
    <div class="card-header" align="center"> Login Panel </div>
    <div class="card-body"> 
    <form action="index.php" method="post">
          <div class="form-group">
              <label>Email Address</label>
              <input type="text" class="form-control form-control-sm" placeholder="Enter Your Email" name="email" required>
          </div>   
          <div class="form-group">
              <label>Password</label>
              <input type="password" class="form-control form-control-sm" placeholder="Enter Your Password" name="password" required>
          </div>          
          <input type="hidden" name="<?php echo $token_id; ?>" value="<?php echo $token_value; ?>" />
         <button type="submit" class="btn btn-sm btn-primary btn-block" name="signin" style="margin-top:10px;border-radius:0px;">Login</button>
</form>
            <a href="" style="color:red;font-size:11px;float:right;">Forgot Password?</a>
    </div>
</div>
     <?php if(isset($_GET['failed'])){ ?>
        <br>
                <div class="alert alert-danger alert-dismissible fade show">
  <strong>Wrong credentials</strong> Please Enter Currect Email And Password
</div>
             <?php	} ?>

            </div>
        </div>
    </div>
</div>

<script>
    $('.alert').fadeOut(3000);

</script>
</body>
</html>