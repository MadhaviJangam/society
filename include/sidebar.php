<!-- Sidebar -->
<?php
	
	$basename = basename($_SERVER['REQUEST_URI']);
	$currentPage = pathinfo($_SERVER['PHP_SELF'], PATHINFO_BASENAME);

	$userPermissionsArray = explode(',',$loggedInUserDetailsArr['permissions']);
?>
<div class="sidebar collapse">
    <div class="sidebar-content">
		<!-- Main navigation -->
		<ul class="navigation">
			<?php 
			if(in_array('country_view',$userPermissionsArray) or in_array('state_view',$userPermissionsArray) or in_array('city_view',$userPermissionsArray) or in_array('locality_view',$userPermissionsArray) or in_array('job_type_view',$userPermissionsArray) or in_array('industry_view',$userPermissionsArray) or in_array('functional_area_view',$userPermissionsArray) or in_array('role_view',$userPermissionsArray) or in_array('skill_view',$userPermissionsArray) or in_array('salary_range_view',$userPermissionsArray) or in_array('degree_view',$userPermissionsArray) or in_array('branch_view',$userPermissionsArray) or in_array('education_type_view',$userPermissionsArray) or in_array('university_view',$userPermissionsArray) or in_array('college_view',$userPermissionsArray) or in_array('position_view',$userPermissionsArray) or in_array('discount_coupon_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
				?>

				<li class="has-ul">
					<a href="#" class="expand"><span>Manage Masters</span> <i class="fa fa-bars pull-right"></i></a>
					<ul class="hidden-ul" style="">
				    <?php
						if(in_array('country_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<li><a href="country-master.php"><span>Country Master</span> <i class="fa fa-globe pull-right"></i></a></li>
				    <?php
			    		}
						if(in_array('state_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<li><a href="state-master.php"><span>State Master</span> <i class="fa fa-thumb-tack pull-right"></i></a></li>
				    <?php
			    		}
						if(in_array('city_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<li><a href="city-master.php"><span>City Master</span> <i class="fa fa-map-marker pull-right"></i></a></li>
				    <?php
			    		}
						if(in_array('locality_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<li><a href="locality-master.php"><span>Locality Master</span> <i class="fa fa-map-pin pull-right"></i></a></li>
				    <?php
			    		}
						if(in_array('job_type_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<li><a href="job-type-master.php"><span>Job Type Master</span> <i class="fa fa-diamond pull-right"></i></a></li>
				    <?php
			    		}
						if(in_array('industry_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<li><a href="industry-master.php"><span>Industry Master</span> <i class="fa fa-industry pull-right"></i></a></li>
				    <?php
			    		}
						if(in_array('functional_area_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<li><a href="functional-area-master.php"><span>Functional Area Master</span> <i class="fa fa-map-signs pull-right"></i></a></li>
				    <?php
			    		}
						if(in_array('role_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<li><a href="role-master.php"><span>Role Master</span> <i class="fa fa-female pull-right"></i></a></li>
				    <?php
			    		}
						if(in_array('skill_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<li><a href="skill-master.php"><span>Skill Master</span> <i class="fa fa-bullseye pull-right"></i></a></li>
				    <?php
			    		}
						if(in_array('salary_range_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<li><a href="salary-range-master.php"><span>Salary Range Master</span> <i class="fa fa-money pull-right"></i></a></li>
				    <?php
			    		}
						if(in_array('degree_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<li><a href="degree-master.php"><span>Degree Master</span> <i class="fa fa-mortar-board pull-right"></i></a></li>
				    <?php
			    		}
						if(in_array('branch_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<li><a href="branch-master.php"><span>Branch/Specialisation Master</span> <i class="fa fa-sitemap pull-right"></i></a></li>
				    <?php
			    		}
						if(in_array('education_type_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<li><a href="education-type-master.php"><span>Education Type Master</span> <i class="fa fa-shield pull-right"></i></a></li>
				    <?php
			    		}
						if(in_array('university_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<li><a href="university-master.php"><span>University Master</span> <i class="fa fa-university pull-right"></i></a></li>
				    <?php
			    		}
						if(in_array('college_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<li><a href="college-master.php"><span>College Master</span> <i class="fa fa-leanpub pull-right"></i></a></li>
				    <?php
			    		}
						if(in_array('position_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<li><a href="position-master.php"><span>Job Position Master</span> <i class="fa fa-cubes pull-right" aria-hidden="true"></i></a></li>
				    <?php
			    		}
						// if(in_array('discount_coupon_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') {
					?>
						<!-- <li><a href="discount-coupon-master.php"><span>Discount Coupon Master</span> <i class="fa fa-percent pull-right"></i></a></li> -->
				    <?php
			    		// }
			    		if(in_array('ethinicity_view', $userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super'){
					?>
						<li><a href="ethinicity-master.php"><span>Ethinicity Master</span> <i class="fa fa-sign-language pull-right"></i></a></li>
					<?php } ?>
					</ul>
				</li>
				<?php 
				}
				if($loggedInUserDetailsArr['user_role']=='super'){
				?>
					<li <?php if($currentPage=='contact-cms-master.php' ){ echo 'class="active"'; } ?> >
						<a href="contact-cms-master.php"><span>CMS Master</span> <i class="fa fa-user-secret"></i></a>
					</li>

			<?php }if(in_array('employer_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') { ?>
					<li <?php if($currentPage=='employer-master.php' || $currentPage=='employer-add.php' ){ echo 'class="active"'; } ?> >
						<a href="employer-master.php"><span>Employer Master</span> <i class="fa fa-user-secret"></i></a>
					</li>
			<?php } ?>

			<?php if(in_array('sales_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') { ?>
					<li <?php if($currentPage=='sales-master.php' || $currentPage=='sales-add.php' ){ echo 'class="active"'; } ?> >
						<a href="sales-master.php"><span>Sales Master</span> <i class="fa fa-user-secret"></i></a>
					</li>
			<?php } ?>

			<?php if(in_array('job-seeker_view',$userPermissionsArray) or $loggedInUserDetailsArr['user_role']=='super') { ?>
				<li <?php if($currentPage=='job-seeker-master.php' || $currentPage=='job-seeker-personal1-add.php'|| $currentPage=='job-seeker-personal2-add.php' ){ echo 'class="active"'; } ?> >
					<a href="job-seeker-master.php"><span>Job Seeker Master</span> <i class="fa fa-street-view"></i></a>
				</li>
			<?php } ?>
			
			<?php	if($loggedInUserDetailsArr['user_role']=='super') { ?>
				<li><a href="user-management.php"><span>User Management Master</span> <i class="fa fa-users"></i></a></li>
			<?php } ?>
			<?php	if($loggedInUserDetailsArr['user_role']=='super') { ?>
				<li><a href="updates-master.php"><span>Updates</span> <i class="fa fa-users"></i></a></li>
			<?php } ?>
			<?php	if($loggedInUserDetailsArr['user_role']=='super') { ?>
				<li><a href="salesEnquiryList.php"><span>Sales Enquires</span> <i class="fa fa-users"></i></a></li>
			<?php } ?>
			

			<li class="has-ul">
				<a href="#" class="expand"><span>Employer Subscription</span> <i class="fa fa-bars pull-right"></i></a>
				<ul class="hidden-ul" style="">
					<?php	if($loggedInUserDetailsArr['user_role']=='super') { ?>
						<li><a href="combo-package-master.php"><span>Combo Packages</span> <i class="fa fa-users"></i></a></li>
					<?php } ?>
					<?php	if($loggedInUserDetailsArr['user_role']=='super') { ?>
						<li><a href="job-posting-master.php"><span>Job Posting Packages</span> <i class="fa fa-users"></i></a></li>
					<?php } ?>
					<?php	if($loggedInUserDetailsArr['user_role']=='super') { ?>
						<li><a href="databaseSearch-master.php"><span>Database Search Packages</span> <i class="fa fa-users"></i></a></li>
					<?php } ?>
					<?php	if($loggedInUserDetailsArr['user_role']=='super') { ?>
						<li><a href="top-up-packages-master.php"><span>top-up-packages-master</span> <i class="fa fa-users"></i></a></li>
					<?php } ?>
					<?php	if($loggedInUserDetailsArr['user_role']=='super') { ?>
						<li><a href="mass-email-master.php"><span>Mass Email Packages</span> <i class="fa fa-users"></i></a></li>
					<?php } ?>
					<?php	if($loggedInUserDetailsArr['user_role']=='super') { ?>
						<li><a href="mass-sms-master.php"><span>Mass SMS Packages</span> <i class="fa fa-users"></i></a></li>
					<?php } ?>
					<?php	if($loggedInUserDetailsArr['user_role']=='super') { ?>
						<li><a href="mass-excel-master.php"><span>Mass Excel Packages</span> <i class="fa fa-users"></i></a></li>
					<?php } ?>
					<?php	if($loggedInUserDetailsArr['user_role']=='super') { ?>
						<li><a href="employer-product.php"><span>Product</span> <i class="fa fa-users"></i></a></li>
					<?php } ?>
				</ul>
			</li>

			<li class="has-ul">
				<a href="#" class="expand"><span>Jobseeker Subscription</span> <i class="fa fa-bars pull-right"></i></a>
				<ul class="hidden-ul" style="">
					<?php	if($loggedInUserDetailsArr['user_role']=='super') { ?>
						<li><a href="js-combo-package-master.php"><span>Combo Packages</span> <i class="fa fa-users"></i></a></li>
					<?php } ?>
					<?php	if($loggedInUserDetailsArr['user_role']=='super') { ?>
						<li><a href="js-portal-specific-services.php"><span>Portal Specific Services</span> <i class="fa fa-users"></i></a></li>
					<?php } ?>
					<?php	if($loggedInUserDetailsArr['user_role']=='super') { ?>
						<li><a href="js-profes-resume-writing.php"><span>Jobseeker Services</span> <i class="fa fa-users"></i></a></li>
					<?php } ?>
				</ul>
			</li>
			<?php	if(1 && $loggedInUserDetailsArr['user_role']=='super') { ?>
				<li><a href="keyword-relation-master.php"><span>Keyword Relation Master</span> <i class="fa fa-users"></i></a></li>
			<?php } ?>
		</ul>
      <!-- /main navigation -->
	</div>
</div>
<!-- /sidebar -->
