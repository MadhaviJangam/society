
<?php
session_start();
  include '../include/admin-functions.php';
  $admin = new AdminFunctions();

$mainPageName='Master';
$mainPageURL='dashboard';
$pageName='Collection Heads';
$pageURL='collection-head';
$tableName='collection_head';
$results = $admin->query("SELECT * FROM ".PREFIX.$tableName." WHERE deleted_time=0 GROUP BY id DESC");

setcookie('page',$pageURL, time() + (86400 * 30), "/");
if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueMemberMasterById($id);
}

?>
<?php if(isset($_GET['registersuccess'])){ ?>
  <script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Success('<?php echo $pageName; ?> successfully Added');
       </script>
<?php } ?>


<?php if(isset($_GET['updatesuccess'])){ ?>

<script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Warning('<?php echo $pageName; ?> successfully updated');
       </script>
<?php } ?>


<?php if(isset($_GET['deletesuccess'])){ ?>
<script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Failure('<?php echo $pageName; ?> successfully deleted');
       </script>

<?php } ?>
<ul class="breadcrumb">
  <li><a href="javascript:void(0);" > <?php echo $mainPageName; ?></a></li>
  <li><a href="javascript:void(0);" data-page="<?php echo $pageURL; ?>"> <?php echo $pageName; ?></a></li>
</ul>

<div class="container-fluid contentsection">
  <div class="row">
    <div class="col-sm-4">
      <div class="card">
        <div class="card-header"> Collection Heads </div>
            <div class="card-body"> 
                <div class="form-group" style="display:none">
                    <label>Code No</label>
                    <input type="text" class="form-control form-control-sm"  name="uname">
                </div>  
                <div class="form-group">
                    <label>SR.No</label>
                    <input type="text" class="form-control form-control-sm"  name="uname">
                </div>  
                <div class="form-group">
                    <label>Description On Bill</label>
                    <textarea type="text" class="form-control form-control-sm"  name="uname"></textarea>
                </div> 
                <div class="form-group">
                    <label>Account Ledger</label>
                    <input type="text" class="form-control form-control-sm"  name="uname">
                </div>  
                <div class="form-group">
                    <label>Short Description On Ledger Tally</label>
                    <input type="text" class="form-control form-control-sm"  name="uname">
                </div> 
            </div>
        </div> 
    </div>
    <div class="col-sm-4">
      <div class="card">
        <div class="card-header"> Calculation Details</div>
          <div class="card-body">
            <div class="form-group">
                <label>Rate Calculation On</label>
            </div> 
            <div class="form-group">
              <input type="radio" name="radio_name">
              <label for="against_ref">SQ.ft</label>
              <input type="radio" name="radio_name">
              <label for="against_ref">Fix</label>
            </div> 
            <div class="form-group">
                <label>Amount</label>
                <input type="text" class="form-control form-control-sm"  name="uname">
            </div>  
            <div class="form-group">
                <label>Interest Calculation</label>
                <input type="radio" name="radio_name">
              <label for="against_ref">Yes</label>
              <input type="radio" name="radio_name">
              <label for="against_ref">No</label>
            </div> 
            <div class="form-group">
                <label>GST Calculation</label>
                <input type="radio" name="radio_name">
              <label for="against_ref">Yes</label>
              <input type="radio" name="radio_name">
              <label for="against_ref">No</label>
            </div> 
            <div class="form-group">
                <label>SAC Code</label>
                <input type="text" class="form-control form-control-sm"  name="sac_code">
            </div> 
            <div class="form-group">
                <label>GST %</label>
                <input type="text" class="form-control form-control-sm"  name="gst">
            </div> 
            <div class="form-group">
                <label>Check Per Month GST Limit</label>
                <input type="radio" name="radio_name">
              <label for="against_ref">Yes</label>
              <input type="radio" name="radio_name">
              <label for="against_ref">No</label>
            </div> 
          </div>
        </div>
      </div>
      
        
         <button class="btn btn-sm btn-primary btn-block" style="margin-top:10px;border-radius:0px;">Add Collection Heads</button>
        </div>
      </div>
    </div>

    

  </div>


</div>

<script>

$('.nav-item').removeClass('active');
$('#master-page').addClass('active');
</script>