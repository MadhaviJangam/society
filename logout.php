<?php
	include_once 'include/config.php';
	include_once 'include/admin-functions.php';
	$admin = new AdminFunctions();
	$admin->logoutSession();
	session_destroy();
	header("location:index.php");
	exit;
	
?>
