<?php
  include 'include/admin-functions.php';
  
  $PageName = "Society Master";
  $pageURL = "dashboard.php";
  $tableName = "society_master";
  $admin = new AdminFunctions();

  $csrf = new csrf();
  $token_id = $csrf->get_token_id();
  $token_value = $csrf->get_token($token_id);


if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: index.php");
	exit();
}

if(isset($_SESSION["database_id"]) && $_SESSION["database_id"]!=''){
	header("location: admin-panel/index.php");
	exit();
}

$results = $admin->query("SELECT * FROM ".PREFIX.$tableName." ");

if(isset($_GET['co_id']) && isset($_GET['so_id'])){
    if($_GET['co_id']==$loggedInUserDetailsArr['id']){
        $_SESSION["society_id"] = $_GET['so_id'];
        $_SESSION["company_id"] = $_GET['co_id'];
        $database_name = $admin->fetch($admin->query("SELECT database_name x FROM ".PREFIX."database_mgt WHERE company_id='".$_GET['co_id']."' AND society_id='".$_GET['so_id']."' "))['x'];
        $_SESSION["database_id"] = $database_name;
        header("location: admin-panel/index.php");
	    exit();
    }
}

if(isset($_POST['register'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addSocietyMaster($_POST,$loggedInUserDetailsArr['id']);
		header("location:".$pageURL."?registersuccess");
	exit();
	}
}
if (isset($_COOKIE['page'])) {
  setcookie('page', null, -1, '/'); 
}
    ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/jquery.dataTables.min.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/validate.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<style>
.nav-link{
    color:black;font-size:15px; 
    width:100%;
}
.material-icons{
    display: inline-flex;
    vertical-align: top;
  
}
.active{
    border-bottom:2px solid #4834d4;
}
@media (min-width: 992px){
	.dropdown-menu .dropdown-toggle:after{
		border-top: .3em solid transparent;
	    border-right: 0;
	    border-bottom: .3em solid transparent;
	    border-left: .3em solid;
	}
	.dropdown-menu .dropdown-menu{
		margin-left:0; margin-right: 0;
	}
	.dropdown-menu li{
		position: relative;
	}
	.nav-item .submenu{ 
		display: none;
		position: absolute;
		left:100%; top:-7px;
	}
	.nav-item .submenu-left{ 
		right:100%; left:auto;
	}
	.dropdown-menu > li:hover{ background-color: #f1f1f1 }
	.dropdown-menu > li:hover > .submenu{
		display: block;
	}
}
 

.loader{
  margin: 0 0 2em;
  height: 100%;
  width: 100%;
  text-align: center;
  padding: 1em;
  margin: 0 auto 1em;
  display: inline-block;
  vertical-align: top;
}

/*
  Set the color of the icon
*/
svg path,
svg rect{
  fill: #FF6700;
}

.card{
border-top:4px solid #8e44ad;
border-radius:0px;
}

.card-header,.card-footer,.card-body {
    padding: 0.4rem 0.4rem;
    background:#fff;
    }
    .contentsection{
        margin-top:1%;
        margin-bottom:1%;
    }
.card-body > .form-group > label{
    font-size:10px;
    margin:1px;
}
.form-group > label{
    font-size:10px;
    margin:1px;
}
.form-group{
    margin:2px;
}
.form-control-sm {
    height: calc(1.5em + .5rem + 2px);
    padding: .2rem .2rem;
    border-radius: .0rem;
    border-bottom:1px solid #7f8c8d;
}
label{
    font-size:10px;
}



</style>
<body style="background:#f4f6fa;font-family: 'Noto Sans JP', sans-serif;">
<nav class="navbar navbar-expand-md navbar-light d-print-none navbardesktop" style="border-bottom:2px solid#eee;background:#FFF;padding:2px;">
  <a class="navbar-brand" href="#" style="color:black;font-size:14px;"><i class="material-icons">location_city</i> UNIQUE SOCIETY</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link btn-sm btn-primary " style="border-radius:0px;color:#fff;text-align:center;margin:2px;" href="<?php echo $pageURL; ?>?add"> Create Society</a>
      </li>   
    </ul>
    <ul class="navbar-nav ml-auto">
      
      <li><a class="nav-link" style="border-bottom:2px solid red;color:red;" href="logout.php"><i class="material-icons">exit_to_app</i> <span>Logout</span></a></li>

    </ul>
  </div>  
</nav>

<div class="container-fluid">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-md-12">
                <br>
                <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>
                <form id="form" method="post">
                <div class="card">

                 <div class="card-header" align="center"> <?php echo $PageName; ?> 
                 <a href="<?php echo $pageURL; ?>" align="right" style="font-size:14px;color:red;float:right;"> <i class="fas fa-hand-point-left"></i> Back</a>

                </div>
                <div class="card-body"> 
              
                    <div class="form-group">
                       <div class="row">
                            <div class="col-md-4">
                                 <label>Society Name<em>*</em> </label>
                                     <input type="text" name="society_name" value="<?php if(isset($_GET['edit'])) { echo $data['supplier_name']; } ?>" class="form-control form-control-sm">
                             </div>

                             <div class="col-md-4">
                                 <label>Society short Name<em>*</em> </label>
                                     <input type="text" name="short_name" value="<?php if(isset($_GET['edit'])) { echo $data['supplier_name']; } ?>" class="form-control form-control-sm">
                             </div>
                              <div class="col-md-4">
                                 <label>Registration No<em>*</em> </label>
                                     <input type="text" name="registration_no" value="<?php if(isset($_GET['edit'])) { echo $data['supplier_name']; } ?>" class="form-control form-control-sm">
                             </div>

                             <div class="col-md-4">
                                 <label>PAN No<em>*</em> </label>
                                     <input type="text" name="pan_no" value="<?php if(isset($_GET['edit'])) { echo $data['supplier_name']; } ?>" class="form-control form-control-sm">
                             </div>

                             <div class="col-md-4">
                                 <label>TAN No<em>*</em> </label>
                                     <input type="text" name="tan_no" value="<?php if(isset($_GET['edit'])) { echo $data['supplier_name']; } ?>" class="form-control form-control-sm">
                             </div>
                             <div class="col-md-4">
                                 <label>GST No<em>*</em> </label>
                                     <input type="text" name="gst_no" value="<?php if(isset($_GET['edit'])) { echo $data['supplier_name']; } ?>" class="form-control form-control-sm">
                             </div>
                        </div>
                    </div>
                  
                </div>
               
                <div class="card-footer" align="center"> 
                    <div class="row">
                 <input type="hidden" name="<?php echo $token_id; ?>" value="<?php echo $token_value; ?>" />
                    <div class="col-sm-6">
                    <?php if(isset($_GET['edit'])){ ?>
                        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>
                        <button type="submit" style="margin:2px;" name="update" value="update" id="update" class="btn btn-warning btn-sm btn-block"><i class="fas fa-save"></i> Update <?php echo $PageName; ?></button>
                        <?php } else { ?>
                        <button type="submit"  style="margin:2px;" name="register" id="register" class="btn btn-success  btn-sm btn-block"><i class="fas fa-save"></i> Add <?php echo $PageName; ?></button>
                        <?php } ?>
                    </div>
                    <div class="col-sm-6">
                        <a  style="margin:2px;" class="btn btn-danger  btn-sm btn-block" href="javascript:void(0);" onclick="clearall()"><i class="fas fa-broom "></i>Clear All</a>
                    </div>
                </div>
                </div>
                </div>
            </form> 
<?php } ?>

            <div class="card">
                <div class="card-header" align="center"> Society Login </div>
                <div class="card-body"> 
                    <div class="form-group">
                        <div class="row">
                           
                            <?php while($row = $admin->fetch($results)){ ?>
                                <div class="col-sm-3">
      <div class="card" style="font-size:12px;border-top:2px solid #ee5253;">
        <div class="card-header"> <i class="material-icons" style="font-size:20px;">location_city</i><a href="dashboard.php?co_id=<?php echo $row['company_id']; ?>&so_id=<?php echo $row['id']; ?>"> <?php echo $row['society_name']; ?> </a> <a href="javascript:void(0);"><i style="font-size:16px;float:right;color:#000;" class="material-icons">edit</i></a></div>
        <div class="card-body">
             <div class="row">
                <div class="col-sm-6"><i style="font-size:20px;" class="material-icons">donut_large</i> <label> Short Name : <?php echo $row['short_name']; ?></label></div>
                <div class="col-sm-6"><i style="font-size:20px;" class="material-icons">donut_large</i> <label> Registration No : <?php echo $row['registration_no']; ?></label></div>
                <div class="col-sm-6"><i style="font-size:20px;" class="material-icons">donut_large</i> <label> PAN No : <?php echo $row['pan_no']; ?></label></div>
                <div class="col-sm-6"><i style="font-size:20px;" class="material-icons">donut_large</i> <label> TAN No : <?php echo $row['tan_no']; ?></label></div>
                <div class="col-sm-6"><i style="font-size:20px;" class="material-icons">donut_large</i> <label> GST NO : <?php echo $row['gst_no']; ?></label></div>

            </div>
            </div>
            
      </div>
    </div>

                                <?php } ?>
                           
                        </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

<script>
    
    $(function () {
 $('#form').validate({
   rules: {
     ignore: [],
         debug: false,
         society_name : {
        required: true,
     },
     short_name : {
        required: true,
     },
     registration_no : {
        required: true,
     },
     pan_no : {
        required: true,
     },
     tan_no : {
        required: true,
     },
     gst_no : {
        required: true,
     },
   },
   messages: {
     email: {
       required: "Please enter a email address",
       email: "Please enter a vaild email address"
     },
   },
   errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.fromerrorcheck').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
 });
});


    </script>
</body>
</html>