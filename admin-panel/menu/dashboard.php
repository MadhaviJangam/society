<?php 
$mainPageName='Home';
$mainPageURL='dashboard';
$pageName='Dashboard';
$pageURL='dashboard';

?>
<ul class="breadcrumb">
  <li><a href="javascript:void(0);" > <?php echo $mainPageName; ?></a></li>
  <li><a href="javascript:void(0);" data-page="<?php echo $pageURL; ?>"> <?php echo $pageName; ?></a></li>
</ul>

<div class="container-fluid">

</div>

<script>

$('.breadcrumb > li > a').on('click', function () {
 
  let pagename = $(this).data('page');
  
  if ($("#" + pagename).length != 0) {
    $('#main-ui').html('');
    $('.loader').show();
    $.ajax({
      type: "GET",
      url: 'menu/' + pagename + '.php',
      success: function (res) {
        $('#main-ui').html(res);
        $('.loader').hide();
      }
    });
  }
})

$('.nav-item').removeClass('active');
$('#<?php echo $mainPageURL;?>').addClass('active');
</script>