<style>
.table-wrapper {
  max-width: 100%;
  overflow: scroll;
}

table {
  position: relative;
  border: 1px solid #ddd;
  border-collapse: collapse;
  width:100%;
  font-size:12px;

}

td, th {
  white-space: nowrap;
  border: 1px solid #ddd;
  padding: 3px;
  text-align: center;
}

th {
  background-color: #eee;
  position: sticky;
  top: -1px;
  z-index: 2;

  &:first-of-type {
    left: 0;
    width:10px;
    z-index: 3;
  }
}

tbody tr td:first-of-type {
  background-color: #eee;
  position: sticky;
  left: -1px;
  text-align: left;
}

.header {
   background-color: #341f97;
   color: white;
}

</style>
<?php $button_ui="Collection Charges"; ?>
<nav class="navbar navbar-expand-md navbar-light d-print-none" style="border-bottom:2px solid #eee;background:#FFF;padding-right:2%;padding-left:2%;padding-bottom:0px;padding-top:0px;">
<ul class="navbar-nav">
    <li class="nav-item active" style="padding-right:10px;">Master </li>
    <li class="nav-item active" style="padding-right:10px;">></li>
    <li class="nav-item active">Society <?php echo $button_ui; ?></li>
  </ul> 
 
</nav>
<div class="container-fluid contentsection">
  
  <div class="row addsection ">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header"> Collection Charges Details</div>
        <div class="card-body"> 
        
<div class="table-wrapper">
  <table>
  <thead>
    <tr>
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>C</th>
      <th>D</th>
      <th>E</th>
      <th>F</th>
      <th>G</th>
      <th>H</th>
    </tr>
  </thead>
  <tbody>
   
    <tr>
      <td>1</td>
      <td class="header">No.</td>
      <td class="header">Stracture</td>
      <td class="header">Unit No</td>
      <td class="header">Name Of Owner</td>
      <td class="header">Open. Arrears</td>
      <td class="header">Open. Int.</td>
      <td class="header">Collection Head1</td>
      <td class="header">Total</td>
    </tr>
    <tr>
      <td>2</td>
      <td>1</td>
      <td>Wing A</td>
      <td>101</td>
      <td>Sameer Bayani</td>
      <td contenteditable='true'>10</td>
      <td contenteditable='true'>10</td>
      <td contenteditable='true'>10</td>
      <td>10</td>
    
    </tr>
  
  </tbody>
</table>
</div>
       
      </div>
    </div>

    

  </div>


</div>

<script>

$('.nav-item').removeClass('active');
$('#master-page').addClass('active');
</script>