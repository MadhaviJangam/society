
<?php
session_start();
  include '../include/admin-functions.php';
  $admin = new AdminFunctions();

$mainPageName='Master';
$mainPageURL='dashboard';
$pageName='Ledger Master';
$pageURL='ledger-master';
$tableName='ledger_master';
$results = $admin->query("SELECT * FROM ".PREFIX.$tableName." WHERE deleted_time=0 GROUP BY id DESC");

setcookie('page',$pageURL, time() + (86400 * 30), "/");
if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueLedgerMasterById($id);
}

?>
<?php if(isset($_GET['registersuccess'])){ ?>
  <script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Success('<?php echo $pageName; ?> successfully Added');
       </script>
<?php } ?>


<?php if(isset($_GET['updatesuccess'])){ ?>

<script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Warning('<?php echo $pageName; ?> successfully updated');
       </script>
<?php } ?>


<?php if(isset($_GET['deletesuccess'])){ ?>
<script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Failure('<?php echo $pageName; ?> successfully deleted');
       </script>

<?php } ?>
<ul class="breadcrumb">
  <li><a href="javascript:void(0);" > <?php echo $mainPageName; ?></a></li>
  <li><a href="javascript:void(0);" data-page="<?php echo $pageURL; ?>"> <?php echo $pageName; ?></a></li>
</ul>


<div class="container-fluid contentsection " id="cardeffect">
<div class="row">
<div class="col-sm-12">
<a href="<?php echo '/admin-panel/index.php?add'?>" class="btn btn-sm btn-primary" style="float:right;margin-bottom:5px;"><i class="material-icons">add</i> add New <?php echo $pageName; ?></a>
</div></div>
  <div class="row addsection " >
    <div class="col-sm-12">
    <?php if(isset($_GET['edit']) OR isset($_GET['add'])){ ?>
    <form id="form" action="/admin-panel/menu/ajax.php" method="post">
      <div class="card" >
        <div class="card-header"> <?php echo $pageName; ?>
        </div>
        <div class="card-body">       
          <div class="row">
            <div class="col-sm-3"> 
                <div class="form-group">
                    <label>Description</label>
                    <input type="text" class="form-control form-control-sm"  name="ledger_name" value="<?php if(isset($_GET['edit'])) { echo $data['ledger_name'];}?>">
                </div>  
            </div>
            <div class="col-sm-2"> 
                <div class="form-group">
                  <label>Income / Expense Head</label>
                  <select class="form-control form-control-sm" name="head_type">
                  <option value="1" <?php if(isset($_GET['edit']) and $data['head_type']=='1') { echo 'selected'; } ?>>Income</option>
                  <option value="0" <?php if(isset($_GET['edit']) and $data['head_type']=='0') { echo 'selected'; } ?>>Expense</option>
                  </select>               
                </div>  
            </div>  
            <div class="col-sm-2"> 
                <div class="form-group">
                  <label>Active</label>
                  <select class="form-control form-control-sm" name="active">
                <option value="1" <?php if(isset($_GET['edit']) and $data['active']=='1') { echo 'selected'; } ?>>Yes
                </option>
                <option value="0" <?php if(isset($_GET['edit']) and $data['active']=='0') { echo 'selected'; } ?>>No
                </option>
                  </select>               
                </div>  
            </div>  
          </div>
        </div>
        <div class="card-footer" align="center"> 
                    <div class="row">
                    <div class="col-sm-6">
                    <input type="hidden" class="form-control" name="ajax" value="<?php echo $pageURL; ?>"/>
                    <?php if(isset($_GET['edit'])){ ?>
                        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>
                        <button type="submit" style="margin:2px;" name="update" value="update" id="update" class="btn btn-warning btn-sm btn-block"><i class="fas fa-save"></i> Update <?php echo $pageName; ?></button>
                        <?php } else { ?>
                        <button type="submit"  style="margin:2px;" name="register" id="register" class="btn btn-success  btn-sm btn-block"><i class="fas fa-save"></i> Add <?php echo $pageName; ?></button>
                        <?php } ?>
                    </div>
                    <div class="col-sm-6">
                        <a  style="margin:2px;" class="btn btn-danger  btn-sm btn-block" href="/admin-panel/index.php" id="clearall"><i class="fas fa-broom "></i>Close</a>
                    </div>
                </div>
                </div>
                
      </div>
      </form>
      <?php } ?>
    </div>
  </div>
  <br>
  <div class="card">
        <div class="card-header"> <?php echo $pageName; ?> List</div>
        <div class="card-body">   
  <table id="example" class="row-border" style="width:100%">
        <thead>
            <tr>
                <th>Sr. No.</th>
                <th>Description</th>
                <th>Income / Expense</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        <?php $x=1; while($row = $admin->fetch($results)){  ?>
            <tr>
                <td><?php echo $x;?></td>
                <td><?php echo $row['ledger_name'];  ?></td>
                <td><?php  if($row['head_type']==1){ echo 'Income'; }elseif($row['head_type']==0){ echo 'Expense'; } ?></td>
                <td><div class="badge badge-<?php echo $row['active'] == '1'?'success':'danger'; ?> ml-2"><?php echo $row['active'] == '1'?'Active':'Inactive'; ?></div></td>
                <td><a href="/admin-panel/index.php?edit&id=<?php echo $row['id']; ?>" class="btn btn-sm btn-warning">Edit</a></td>
            </tr>
            <?php $x++; } ?>
        </tbody>
        </div>
      </div>    
       
    </table>
   
  
</div>
<script>
   $(function () {
 $('#form').validate({
   rules: {
     ignore: [],
     debug: false,
     ledger_name : {
        required: true,
     },
     active : {
        required: true,
     },
   },
   messages: {
     email: {
       required: "Please enter a email address",
       email: "Please enter a vaild email address"
     },
   },
   errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.fromerrorcheck').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    },
    submitHandler: function(form) {
      $('#update').hide();
      $('#register').hide();
      $('#clearall').hide();
    $.ajax({
        url: form.action,
        type: form.method,
        data: $(form).serialize(),
        success: function(response) {
          window.location.href = '/admin-panel/index.php?msg='+response;
        }            
    });
}
 });
});

$('.nav-item').removeClass('active');
$('#master-page').addClass('active');
$(document).ready(function() {
  
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
} );
     
</script>