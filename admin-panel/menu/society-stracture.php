<?php $button_ui="Stracture"; ?>
<nav class="navbar navbar-expand-md navbar-light d-print-none" style="border-bottom:2px solid #eee;background:#FFF;padding-right:2%;padding-left:2%;padding-bottom:0px;padding-top:0px;">
<ul class="navbar-nav">
    <li class="nav-item active" style="padding-right:10px;">Master </li>
    <li class="nav-item active" style="padding-right:10px;">></li>
    <li class="nav-item active">Society <?php echo $button_ui; ?></li>
  </ul> 

</nav>
<div class="container-fluid contentsection">
  
  <div class="row">

    <div class="col-sm-3">
      <div class="card">
        <div class="card-header"> Wing Details </div>
        <div class="card-body"> 
          <div class="form-group">
              <label>Wing Name</label>
              <input type="text" class="form-control form-control-sm" placeholder="Wing Name" name="uname">
          </div>  
          <div class="form-group">
              <label>Total Flats</label>
              <input type="text" class="form-control form-control-sm" placeholder="Total Flats" name="uname">
          </div>         
         <button class="btn btn-sm btn-primary btn-block" style="margin-top:10px;border-radius:0px;">Add Wing</button>
        </div>
      </div>
    </div>

    <div class="col-sm-3">
      <div class="card">
        <div class="card-header"> Shop Details </div>
        <div class="card-body"> 
          <div class="form-group">
              <label>Total Shops</label>
              <input type="text" class="form-control form-control-sm" placeholder="Total Shops" name="uname">
          </div>          
         <button class="btn btn-sm btn-warning btn-block" style="margin-top:10px;border-radius:0px;">Update Shop</button>
        </div>
      </div>
    </div>

    <div class="col-sm-3">
      <div class="card">
        <div class="card-header"> Office Details </div>
        <div class="card-body"> 
          <div class="form-group">
              <label>Total Office</label>
              <input type="text" class="form-control form-control-sm" placeholder="Total Office" name="uname">
          </div>          
         <button class="btn btn-sm btn-warning btn-block" style="margin-top:10px;border-radius:0px;">Update Office</button>
        </div>
      </div>
    </div>

    <div class="col-sm-3">
      <div class="card">
        <div class="card-header"> Garage Details </div>
        <div class="card-body"> 
          <div class="form-group">
              <label>Total Garage</label>
              <input type="text" class="form-control form-control-sm" placeholder="Total Garage" name="uname">
          </div>          
         <button class="btn btn-sm btn-warning btn-block" style="margin-top:10px;border-radius:0px;">Update Garage</button>
        </div>
      </div>
    </div>

  </div>


</div>

<script>

$('.nav-link').removeClass('active');
//$('#master-page').addClass('active');
</script>