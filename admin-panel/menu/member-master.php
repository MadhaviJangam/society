
<?php
session_start();
  include '../include/admin-functions.php';
  $admin = new AdminFunctions();

$mainPageName='Master';
$mainPageURL='dashboard';
$pageName='Member Master';
$pageURL='member-master';
$tableName='member_master';
$results = $admin->query("SELECT * FROM ".PREFIX.$tableName." WHERE deleted_time=0 GROUP BY id DESC");

setcookie('page',$pageURL, time() + (86400 * 30), "/");
if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueMemberMasterById($id);
}
?>
<?php if(isset($_GET['registersuccess'])){ ?>
  <script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Success('<?php echo $pageName; ?> successfully Added');
       </script>
<?php } ?>


<?php if(isset($_GET['updatesuccess'])){ ?>

<script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Warning('<?php echo $pageName; ?> successfully updated');
       </script>
<?php } ?>


<?php if(isset($_GET['deletesuccess'])){ ?>
<script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Failure('<?php echo $pageName; ?> successfully deleted');
       </script>

<?php } ?>
<ul class="breadcrumb">
  <li><a href="javascript:void(0);" > <?php echo $mainPageName; ?></a></li>
  <li><a href="javascript:void(0);" data-page="<?php echo $pageURL; ?>"> <?php echo $pageName; ?></a></li>
</ul>

<div class="container-fluid contentsection " id="cardeffect">
  <div class="row">
    <div class="col-sm-12">
        <a href="<?php echo '/admin-panel/index.php?add'?>" class="btn btn-sm btn-primary" style="float:right;margin-bottom:5px;"><i class="material-icons">add</i> add New <?php echo $pageName; ?></a>
    </div>
</div>
<?php if(isset($_GET['edit']) OR isset($_GET['add'])){ ?>
    <form id="form" action="/admin-panel/menu/ajax.php" method="post">
    <div class="row " >
      <div class="col-sm-4">
        <div class="card">
          <div class="card-header"> Members Details</div>
          <div class="card-body"> 
            <div class="form-group">
                <label>Stracture</label>
                    <select class="form-control form-control-sm" name="space_type">
                      <option value="0" <?php if(isset($_GET['edit']) and $data['space_type']=='0'){echo 'selected';}?>>Wing A</option>
                      <option value="1" <?php if(isset($_GET['edit']) and $data['space_type']=='1'){echo 'selected';}?>>Wing B</option>
                      <option value="2" <?php if(isset($_GET['edit']) and $data['space_type']=='2'){echo 'selected';}?>>Shop</option>
                      <option value="3" <?php if(isset($_GET['edit']) and $data['space_type']=='3'){echo 'selected';}?>>Office</option>
                      <option value="4" <?php if(isset($_GET['edit']) and $data['space_type']=='4'){echo 'selected';}?>>Garege</option>
                    </select>
            </div>
            <div class="form-group">
                <label>Unit Number</label>
                <input type="text" class="form-control form-control-sm" style="text-align:right;" placeholder="Unit Number" name="unit_no" value="<?php if(isset($_GET['edit'])){ echo $data['unit_no'];} ?>" >
            </div>  
            <div class="form-group">
                <label>Name of owner</label>
                <input type="text" class="form-control form-control-sm" placeholder="Name of owner" name="owner_name" value="<?php if(isset($_GET['edit'])){ echo $data['owner_name'];} ?>" >
            </div>  
            <div class="form-group">
                <label>Admission Date</label>
                <input type="date" class="form-control form-control-sm" placeholder="Date" name="admission_date" value="<?php if(isset($_GET['edit'])){ echo $data['admission_date'];}else { echo date("Y-m-d");}?>">
            </div> 
            <div class="form-group">
                  <label>SQ.Ft Area</label>
                  <input type="text" class="form-control form-control-sm" placeholder="SQ.Ft Area" name="sq_ft_area" value="<?php if(isset($_GET['edit'])){ echo $data['sq_ft_area'];} ?>">
            </div>  
          </div>
        </div>
      </div>
     
      <div class="col-sm-4">
        <div class="card">
          <div class="card-header"> Basic Details</div>
          <div class="card-body">
            <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control form-control-sm" placeholder="Email" name="email_id" value="<?php if(isset($_GET['edit'])){ echo $data['email_id'];} ?>">
            </div> 
            <div class="form-group">
                <label>Phone No./ Whatsapp No.</label>
                <input type="text" class="form-control form-control-sm" placeholder="Phone" name="phone_no" value="<?php if(isset($_GET['edit'])){ echo $data['phone_no'];} ?>">
            </div>  
            <div class="form-group">
                <label>GST No</label>
                <input type="text" class="form-control form-control-sm" placeholder="GST No" name="gst_no" value="<?php if(isset($_GET['edit'])){ echo $data['gst_no'];} ?>">
            </div>  
            <div class="form-group">
                <label>PAN No</label>
                <input type="text" class="form-control form-control-sm" placeholder="PAN No" name="pan_no" value="<?php if(isset($_GET['edit'])){ echo $data['pan_no'];} ?>" >
            </div>  
            <div class="form-group">
                <label>Parking Slot No</label>
                <input type="text" class="form-control form-control-sm" style="text-align:right;" placeholder="Parking Slot No" name="parking_slot_no" value="<?php if(isset($_GET['edit'])){ echo $data['parking_slot_no'];} ?>">
            </div>  
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="card">
            <div class="card-body">
              <div class="form-group">
                  <label>Parking Charges</label>
                  <input type="text" class="form-control form-control-sm" style="text-align:right;" placeholder="Parking Changes" name="parking_charges" value="<?php if(isset($_GET['edit'])){ echo $data['parking_charges'];} ?>" >
              </div>  
               
              <div class="form-group">
                  <label>Lein Bank / Branch</label>
                  <input type="text" class="form-control form-control-sm" placeholder="Lein Bank / Branch" name="lain_bank_branch" value="<?php if(isset($_GET['edit'])){ echo $data['lain_bank_branch'];} ?>" >
              </div>  
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>From Date</label>
                      <input type="date" class="form-control form-control-sm" placeholder="From Date" name="from_date" value="<?php if(isset($_GET['edit'])){ echo $data['from_date'];}else {echo date("Y-m-d");}?>">
                  </div> 
                </div> 
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>To Date</label>
                      <input type="date" class="form-control form-control-sm" placeholder="To Date" name="from_to" value="<?php if(isset($_GET['edit'])){ echo $data['from_to'];}else{ echo date("Y-m-d");}?>">
                  </div>  
                </div>  
              </div>
              <div class="form-group">
                  <label>NOC Details</label>
                  <input type="text" class="form-control form-control-sm" placeholder="NOC Details" name="noc_details" value="<?php if(isset($_GET['edit'])){ echo $data['noc_details'];} ?>" >
              </div>  
              <div class="form-group">
                  <label>Tenent</label>
                    <select class="form-control form-control-sm" name="tenent" >
                        <option value="1" <?php if(isset($_GET['edit']) and $data['tenent']=='1'){echo 'selected';}?>>No</option>
                        <option value="0" <?php if(isset($_GET['edit']) and $data['tenent']=='0'){echo 'selected';}?>>Yes</option>
                    </select>
              </div>  
            </div>
          </div>
      </div>
    </div>
  
    <div class="row">
      <div class="col-sm-4">
      <lable>Regular Bill Opening Balance</lable>
      <br>
      <div class="row">
        <div class="col-sm-6">
          <div class="form-group">
              <label>Principal</label>
              <input type="text" class="form-control form-control-sm" placeholder="" name="uname">
          </div> 
        </div> 
        <div class="col-sm-6">
          <div class="form-group">
              <label>Interest</label>
              <input type="text" class="form-control form-control-sm" placeholder="" name="uname">
          </div>  
        </div>  
      </div>
     
      </div>
      </div>
<br>
      <table id="example1" class="table-bordered table-striped" style="width:100%;border-radius:5px;">
        <thead>
          <tr>
            <th>Sr. No.</th>
            <th>Expense Head</th>
            <th>Amount</th>
          </tr>
        </thead>
      </table>
        <div class="row">
          <div class="col-sm-6">
            <input type="hidden" class="form-control" name="ajax" value="<?php echo $pageURL; ?>"/>
            <?php if(isset($_GET['edit'])){ ?>
                <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>
                <button type="submit" style="margin:2px;" name="update" value="update" id="update" class="btn btn-warning btn-sm btn-block"><i class="fas fa-save"></i> Update <?php echo $pageName; ?></button>
                <?php } else { ?>
                <button type="submit"  style="margin:2px;" name="register" id="register" class="btn btn-success  btn-sm btn-block"><i class="fas fa-save"></i> Add <?php echo $pageName; ?></button>
                <?php } ?>
          </div>
          <div class="col-sm-6">
              <a  style="margin:2px;" class="btn btn-danger  btn-sm btn-block" href="/admin-panel/index.php" id="clearall"><i class="fas fa-broom "></i>Close</a>
          </div>
        </div>
    </form> 
        
      <?php } ?>
      </div>
    </div>
        
  <div class="card">
        <div class="card-header"> <?php echo $pageName; ?> List</div>
        <div class="card-body">   
  <table id="example" class="row-border" style="width:100%">
        <thead>
            <tr>
                <th>Sr. No.</th>
                <th>Stracture</th> 
                <th>Unit Number</th>
                <th>Name of owner</th>
                <th>Admission Date</th>
                <th>Phone No</th>
                <th>GST No</th>
                <th>PAN No</th>
                <th>Tenent</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        <?php $x=1; while($row = $admin->fetch($results)){  ?>
            <tr>
                <td><?php echo $x;?></td>
                <td><?php  if($row['space_type']==0){ echo 'Wing A'; }elseif($row['space_type']==1){ echo 'Wing B'; } elseif($row['space_type']==2){ echo 'Shop'; } elseif($row['space_type']==3){ echo 'Office'; }elseif($row['space_type']==4){ echo 'Garege'; } ?></td>
                <td><?php echo $row['unit_no'];  ?></td>
                <td><?php echo $row['owner_name'];  ?></td>
                <td><?php echo $row['admission_date'];  ?></td>
                <td><?php echo $row['phone_no'];  ?></td>
                <td><?php echo $row['gst_no'];  ?></td>
                <td><?php echo $row['pan_no'];  ?></td>
                <td><?php if($row['tenent']==1){ echo 'No'; }elseif($row['tenent']==0){ echo 'Yes';}  ?></td>
                <td><a href="/admin-panel/index.php?edit&id=<?php echo $row['id']; ?>" class="btn btn-sm btn-warning">Edit</a></td>
            </tr>
            <?php $x++; } ?>
        </tbody>
        </div>
      </div>    
       
    </table>
   
  
</div>
<script>
   $(function () {
 $('#form').validate({
   rules: {
     ignore: [],
     debug: false,
     unit_no : {
        required: true,
     },
     owner_name : {
        required: true,
     },
   },
   messages: {
     email: {
       required: "Please enter a email address",
       email: "Please enter a vaild email address"
     },
   },
   errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.fromerrorcheck').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    },
    submitHandler: function(form) {
      $('#update').hide();
      $('#register').hide();
      $('#clearall').hide();
    $.ajax({
        url: form.action,
        type: form.method,
        data: $(form).serialize(),
        success: function(response) {
          window.location.href = '/admin-panel/index.php?msg='+response;
        }            
    });
}
 });
});

$('.nav-item').removeClass('active');
$('#master-page').addClass('active');
$(document).ready(function() {
  
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
} );
     
</script>