<?php 
session_start();
  include '../include/admin-functions.php';
  $admin = new AdminFunctions();
  if($_POST['ajax']=='parking-rate-master' and isset($_POST['register'])){
    $result = $admin -> addParkingRateMaster($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'registersuccess';  
  }

  if($_POST['ajax']=='parking-rate-master' and isset($_POST['update'])){
    $result = $admin -> updateParkingRateMaster($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'updatesuccess';
  }

  if($_POST['ajax']=='customer-master' and isset($_POST['register'])){
    $result = $admin -> addCustomerMaster($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'registersuccess';  
  }

  if($_POST['ajax']=='customer-master' and isset($_POST['update'])){
    $result = $admin -> updateCustomerMaster($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'updatesuccess';
  }
  if($_POST['ajax']=='supplier-master' and isset($_POST['register'])){
    $result = $admin -> addSupplierMaster($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'registersuccess';  
  }

  if($_POST['ajax']=='supplier-master' and isset($_POST['update'])){
    $result = $admin -> updateSupplierMaster($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'updatesuccess';
  }
  if($_POST['ajax']=='additional-charges-details' and isset($_POST['register'])){
    $result = $admin -> addAdditionalChargesMaster($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'registersuccess';  
  }

  if($_POST['ajax']=='additional-charges-details' and isset($_POST['update'])){
    $result = $admin -> updateAdditionalChargesMaster($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'updatesuccess';
  }
  if($_POST['ajax']=='bank-master' and isset($_POST['register'])){
    $result = $admin -> addBankMaster($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'registersuccess';  
  }

  if($_POST['ajax']=='bank-master' and isset($_POST['update'])){
    $result = $admin -> updateBankMaster($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'updatesuccess';
  }
  if($_POST['ajax']=='group-master' and isset($_POST['register'])){
    $result = $admin -> addGroupMaster($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'registersuccess';  
  }

  if($_POST['ajax']=='group-master' and isset($_POST['update'])){
    $result = $admin -> updateGroupMaster($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'updatesuccess';
  }
  if($_POST['ajax']=='ledger-master' and isset($_POST['register'])){
    $result = $admin -> addLedgerMaster($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'registersuccess';  
  }

  if($_POST['ajax']=='ledger-master' and isset($_POST['update'])){
    $result = $admin -> updateLedgerMaster($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'updatesuccess';
  }
  if($_POST['ajax']=='member-master' and isset($_POST['register'])){
    $result = $admin -> addMemberMaster($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'registersuccess';  
  }

  if($_POST['ajax']=='member-master' and isset($_POST['update'])){
    $result = $admin -> updateMemberMaster($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'updatesuccess';
  }
  if($_POST['ajax']=='collection-heads' and isset($_POST['register'])){
    $result = $admin -> addCollectionHead($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'registersuccess';  
  }

  if($_POST['ajax']=='collection-heads' and isset($_POST['update'])){
    $result = $admin -> updateCollectionHead($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'updatesuccess';
  }
  if($_POST['ajax']=='society-settings-master' and isset($_POST['update'])){
    $result = $admin -> updateSociety($_POST,$_SESSION['company_id'],$_SESSION['society_id'],$_SESSION['company_id']);
    echo 'updatesuccess';
  }
?>