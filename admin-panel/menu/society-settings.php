<?php
session_start();
  include '../include/admin-functions.php';
  $admin = new AdminFunctions();

$mainPageName='Master';
$mainPageURL='dashboard';
$pageName='Society Setting Master';
$pageURL='society-settings-master';
$tableName='society_master';
$results = $admin->query("SELECT * FROM ".PREFIX.$tableName." WHERE deleted_time=0 GROUP BY id DESC");

setcookie('page',$pageURL, time() + (86400 * 30), "/");
if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
  $data = $admin->getUniqueSocietyMasterById($society_id);
  
  print_r($data);
}

?>
<style>
</style>
<nav class="navbar navbar-expand-md navbar-light d-print-none" style="border-bottom:2px solid #eee;background:#FFF;padding-right:2%;padding-left:2%;padding-bottom:0px;padding-top:0px;">
<ul class="navbar-nav">
    <li class="nav-item active" style="padding-right:10px;">Master </li>
    <li class="nav-item active" style="padding-right:10px;">></li>
    <li class="nav-item active">Society Settings</li>
  </ul> 
</nav>
<div class="container-fluid contentsection">
  <div class="row">
    <div class="col-sm-3">
      <div class="card">
        <div class="card-header"> Basic Details</div>
        <div class="card-body">
          <div class="form-group">
              <label>Society Name</label>
              <input type="text" class="form-control form-control-sm" placeholder="Society Name" name="society_name" value="<?php if(isset($_GET['edit'])){ echo $data['society_name'];}?>">
          </div>
          <div class="form-group">
              <label>Society Short Name</label>
              <input type="text" class="form-control form-control-sm" placeholder="Society Short Name" name="society_short_name" value="<?php if(isset($_GET['edit'])){ echo $data['society_short_name'];}?>">
          </div>
          <div class="form-group">
              <label>Address</label>
              <textarea  class="form-control form-control-sm" placeholder="Address" name="society_address"><?php if(isset($_GET['edit'])){ echo $data['society_address'];}?></textarea>
          </div>
          <div class="form-group">
              <label>Registration No.</label>
              <input type="text" class="form-control form-control-sm" placeholder="Registration No." name="registration_no" value="<?php if(isset($_GET['edit'])){ echo $data['registration_no'];}?>">
          </div>
          <div class="form-group">
              <label>Date of Incorp.</label>
              <input type="date" class="form-control form-control-sm" placeholder="Date of Incorp." name="date_of_incorp" value="<?php if(isset($_GET['edit'])){ echo $data['date_of_incorp'];}else{ echo date("Y-m-d");}?>">
          </div>
          <div class="form-group">
              <label>PAN No.</label>
              <input type="text" class="form-control form-control-sm" placeholder="PAN No." name="pan_no" value="<?php if(isset($_GET['edit'])){ echo $data['pan_no'];}?>">
          </div>
          <div class="form-group">
              <label>TAN No.</label>
              <input type="text" class="form-control form-control-sm" placeholder="TAN No." name="tan_no" value="<?php if(isset($_GET['edit'])){ echo $data['tan_no'];}?>">
          </div>
          <div class="form-group">
              <label>Telephone No.</label>
              <input type="text" class="form-control form-control-sm" placeholder="Telephone No." name="telephone_no" value="<?php if(isset($_GET['edit'])){ echo $data['telephone_no'];}?>">
          </div>
          <div class="form-group">
              <label>Email ID</label>
              <input type="text" class="form-control form-control-sm" placeholder="Email ID" name="email_id" value="<?php if(isset($_GET['edit'])){ echo $data['email_id'];}?>">
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-3">
      <div class="card">
        <div class="card-header"> Basic Details</div>
        <div class="card-body">
          <div class="form-group">
                <label>GST No.</label>
                <input type="text" class="form-control form-control-sm" placeholder="GST No." name="gst_no" value="<?php if(isset($_GET['edit'])){ echo $data['gst_no'];}?>">
          </div>
          <div class="form-group">
              <label>GST Limit Per Month</label>
              <input type="text" class="form-control form-control-sm" placeholder="GST Limit Per Month" name="gst_limit_per_month" value="<?php if(isset($_GET['edit'])){ echo $data['gst_limit_per_month'];}?>">
          </div>
          <div class="form-group">
              <label>GST Applicable</label>
                <select class="form-control form-control-sm" name="gst_applicable">
                  <option value="1" <?php if(isset($_GET['edit']) and $data['gst_applicable']=='1'){ echo 'selected';} ?>>No</option>
                  <option value="0" <?php if(isset($_GET['edit']) and $data['gst_applicable']=='0'){ echo 'selected';} ?>>Yes</option>
              </select>
          </div>
        </div>
      </div>
<br>
      <div class="card">
        <div class="card-header"> Bank Details</div>
        <div class="card-body">
          <div class="form-group">
              <label>Bank Name</label>
              <input type="text" class="form-control form-control-sm" placeholder="Bank Name" name="bank_name" value="<?php if(isset($_GET['edit'])){ echo $data['bank_name'];}?>">
          </div>
          <div class="form-group">
              <label>Branch Name</label>
              <input type="text" class="form-control form-control-sm" placeholder="Branch Name" name="branch_name" value="<?php if(isset($_GET['edit'])){ echo $data['branch_name'];}?>">
          </div>
          <div class="form-group">
              <label>A/c No.</label>
              <input type="text" class="form-control form-control-sm" placeholder="A/c No" name="account_no" value="<?php if(isset($_GET['edit'])){ echo $data['account_no'];}?>">
          </div>
          <div class="form-group">
              <label>IFSC No</label>
              <input type="text" class="form-control form-control-sm" placeholder="IFSC No" name="ifsc_no" value="<?php if(isset($_GET['edit'])){ echo $data['ifsc_no'];}?>">
          </div>
        </div>
      </div>
    </div>
  
    <div class="col-sm-3">
      <div class="card">
        <div class="card-header"> Interest Details</div>
        <div class="card-body">
          <div class="form-group">
              <label>Interest Calc.</label>
                <select class="form-control form-control-sm" name="intrest_calc">
                  <option value="1" <?php if(isset($_GET['edit']) and $data['intrest_calc']=='1'){ echo 'selected';} ?>>No</option>
                  <option value="0" <?php if(isset($_GET['edit']) and $data['intrest_calc']=='0'){ echo 'selected';} ?>>Yes</option>
                </select>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Interest %</label>
                <input type="text" class="form-control form-control-sm" placeholder="Interest %" name="intrest_per" value="<?php if(isset($_GET['edit'])){ echo $data['intrest_per'];}?>">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>After Amount</label>
                <input type="text" class="form-control form-control-sm" placeholder="After Amount" name="intrest_per_after_amount" value="<?php if(isset($_GET['edit'])){ echo $data['intrest_per_after_amount'];}?>">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Fix Amount</label>
                <input type="text" class="form-control form-control-sm" placeholder="Fix Amount" name="fix_amount" value="<?php if(isset($_GET['edit'])){ echo $data['fix_amount'];}?>">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>After Amount</label>
                <input type="text" class="form-control form-control-sm" placeholder="After Amount" name="fix_amount_after_amount" value="<?php if(isset($_GET['edit'])){ echo $data['fix_amount_after_amount'];}?>">
              </div>
            </div>
          </div>
          </div>
   </div>
   </br>

      <div class="card">
        <div class="card-header"> Bill Details</div>
        <div class="card-body">
          <div class="form-group">
            <label>Bill Format</label>
              <select class="form-control form-control-sm" name="bill_format">
                  <option value="Bill Cum Receipt Format With GST">Bill Cum Receipt Format With GST</option>
                  <option value="Bill Cum Receipt Format Without GST">Bill Cum Receipt Format Without GST</option>
                  <option value="Single Bill on A4 Page GST">Single Bill on A4 Page GST</option>
                  <option value="Single Bill on A4 Page">Single Bill on A4 Page</option>
                  <option value="Two Bills on A4 Page">Two Bills on A4 Page</option>
              </select>
          </div>
          <div class="form-group">
              <label><b>Print On The Bill ---------------------</b></label>
          </div>
          <div class="form-group">
              <input type="radio" name="print_on_bill" value="first_name" <?php if(isset($_GET['edit']) && $data['print_on_bill']=='first_name'){ echo "checked";}  ?>>
              <label for="first_name">Only First Name to be Print on the Bill</label>
          </div>
          <div class="form-group">
              <input type="radio" name="print_on_bill" value="last_name" <?php if(isset($_GET['edit']) && $data['print_on_bill']=='last_name'){ echo "checked";}  ?>>
              <label for="last_name">All Names to be Print on the Bill</label>
          </div>
          <div class="form-group">
              <label><b>Billing Cycle Information</b></label>
          </div>
          <div class="form-group">
              <input type="radio" name="billing_cycle" value="monthly" <?php if(isset($_GET['edit']) && $data['billing_cycle']=='monthly'){ echo "checked";}  ?>>
              <label for="monthly">Monthly</label>
              <input type="radio" name="billing_cycle" value="bi_monthly" <?php if(isset($_GET['edit']) && $data['billing_cycle']=='bi_monthly'){ echo "checked";}  ?>>
              <label for="bi_monthly">Bi-Monthly</label>
              <input type="radio" name="billing_cycle" value="quartly" <?php if(isset($_GET['edit']) && $data['billing_cycle']=='quartly'){ echo "checked";}  ?>>
              <label for="=quartly">Quartly</label>
              <input type="radio" name="billing_cycle" value="half_yearly" <?php if(isset($_GET['edit']) && $data['billing_cycle']=='half_yearly'){ echo "checked";}  ?>>
              <label for="half_yearly">Half-Yearly</label>
          </div>
          <div class="form-group">
              <label><b>Receipt To be Adjust Towards</b></label>
          </div>
          <div class="form-group">
              <input type="radio" name="receipt_to_be_adjust" value="first_interest" <?php if(isset($_GET['edit']) && $data['receipt_to_be_adjust']=='first_interest'){ echo "checked";}  ?>>
              <label for="first_interest">First Interest And Next Principal Amount</label>
          </div>
          <div class="form-group">
              <input type="radio" name="receipt_to_be_adjust" value="first_principal" <?php if(isset($_GET['edit']) && $data['receipt_to_be_adjust']=='first_principal'){ echo "checked";}  ?>>
              <label for="first_principal">First Principal Amount And Next Interest Amount</label>
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-3">
      <div class="card">
        <div class="card-header"> Rebate Details</div>
        <div class="card-body">
        <div class="form-group">
              <label>Rebate Calc.</label>
              <select class="form-control form-control-sm" name="rebate_calc">
                  <option value="1" <?php if(isset($_GET['edit']) and $data['rebate_calc']=='1'){ echo 'selected';} ?>>No</option>
                  <option value="0" <?php if(isset($_GET['edit']) and $data['rebate_calc']=='0'){ echo 'selected';} ?>>Yes</option>
                </select>
          </div>

          <div class="form-group">
              <label>Only in April ?</label>
              <select class="form-control form-control-sm" name="only_in_april">
                  <option value="1" <?php if(isset($_GET['edit']) and $data['only_in_april']=='1'){ echo 'selected';} ?>>No</option>
                  <option value="0" <?php if(isset($_GET['edit']) and $data['only_in_april']=='0'){ echo 'selected';} ?>>Yes</option>
                </select>          
          </div>

          <div class="form-group">
              <label>Zero Pending ?</label>
              <select class="form-control form-control-sm" name="zero_pending">
                  <option value="1" <?php if(isset($_GET['edit']) and $data['zero_pending']=='1'){ echo 'selected';} ?>>No</option>
                  <option value="0" <?php if(isset($_GET['edit']) and $data['zero_pending']=='0'){ echo 'selected';} ?>>Yes</option>
                </select>
          </div>

          <div class="form-group">
              <label>Rebate-1 Days</label>
              <input type="text" class="form-control form-control-sm" placeholder="Rebate-1 Days" name="rebate_one_day" value="<?php if(isset($_GET['edit'])){ echo $data['rebate_one_day'];}?>">
          </div>

          <div class="form-group">
              <label>Rebate %</label>
              <input type="text" class="form-control form-control-sm" placeholder="Rebate %" name="rebate_one_day_per" value="<?php if(isset($_GET['edit'])){ echo $data['rebate_one_day_per'];}?>">
          </div>

          <div class="form-group">
              <label>Rebate-2 Days</label>
              <input type="text" class="form-control form-control-sm" placeholder="Rebate-2 Days" name="rebate_two_day" value="<?php if(isset($_GET['edit'])){ echo $data['rebate_two_day'];}?>">
          </div>

          <div class="form-group">
              <label>Rebate %</label>
              <input type="text" class="form-control form-control-sm" placeholder="Rebate %" name="rebate_two_per" value="<?php if(isset($_GET['edit'])){ echo $data['rebate_two_per'];}?>">
          </div>
       

        </div>
      </div>
  <br>
      <div class="card">
        <div class="card-body">
        <input type="hidden" class="form-control" name="ajax" value="<?php echo $pageURL; ?>"/>
            <button type="submit" style="margin:2px;" name="update" value="update" id="update" class="btn btn-warning btn-sm btn-block"><i class="fas fa-save"></i> Update <?php echo $pageName; ?></button>
        </div>
      </div>
    </div>
  </div>
  
</div>
<div class="col-sm-6">
  <div class="form-group">
      <label size:14px;><b>Foot Note To Be Print On Bill</b></label>
      <input type="text" class="form-control form-control-sm" placeholder="" name="foot_note" value="<?php if(isset($_GET['edit'])){ echo $data['foot_note'];}?>">
  </div>
  </div>
<script>

   $(function () {
 $('#form').validate({
   rules: {
     ignore: [],
     debug: false,
     fix_amount : {
        required: true,
     },
     address : {
        required: true,
     },
     pan_no : {
        required: true,
     },
     state_code : {
        required: true,
     },
     opening_balance : {
        required: true,
     },
     opening_balance_date : {
        required: true,
     },
     sac_code : {
        required: true,
     },
   },
   messages: {
     email: {
       required: "Please enter a email address",
       email: "Please enter a vaild email address"
     },
   },
   errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.fromerrorcheck').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    },
    submitHandler: function(form) {
      $('#update').hide();
      $('#register').hide();
      $('#clearall').hide();
    $.ajax({
        url: form.action,
        type: form.method,
        data: $(form).serialize(),
        success: function(response) {
          window.location.href = '/admin-panel/index.php?msg='+response;
        }            
    });
}
 });
});

$('.nav-link').removeClass('active');
</script>