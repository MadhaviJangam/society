
<?php
session_start();
  include '../include/admin-functions.php';
  $admin = new AdminFunctions();

$mainPageName='Master';
$mainPageURL='dashboard';
$pageName='Collection Heads';
$pageURL='collection-heads';
$tableName='collection_head';

$getActiveLedgerDetails = $admin->getActiveLedgerDetails();
$results = $admin->query("SELECT * FROM ".PREFIX.$tableName." WHERE deleted_time=0 GROUP BY id DESC");

setcookie('page',$pageURL, time() + (86400 * 30), "/");
if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueCollectionHeadById($id);
}
?>
<?php if(isset($_GET['registersuccess'])){ ?>
  <script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Success('<?php echo $pageName; ?> successfully Added');
       </script>
<?php } ?>


<?php if(isset($_GET['updatesuccess'])){ ?>

<script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Warning('<?php echo $pageName; ?> successfully updated');
       </script>
<?php } ?>


<?php if(isset($_GET['deletesuccess'])){ ?>
<script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Failure('<?php echo $pageName; ?> successfully deleted');
       </script>

<?php } ?>
<ul class="breadcrumb">
  <li><a href="javascript:void(0);" > <?php echo $mainPageName; ?></a></li>
  <li><a href="javascript:void(0);" data-page="<?php echo $pageURL; ?>"> <?php echo $pageName; ?></a></li>
</ul>

<div class="container-fluid contentsection " id="cardeffect">
  <div class="row">
    <div class="col-sm-12">
        <a href="<?php echo '/admin-panel/index.php?add'?>" class="btn btn-sm btn-primary" style="float:right;margin-bottom:5px;"><i class="material-icons">add</i> add New <?php echo $pageName; ?></a>
    </div>
</div>
<?php if(isset($_GET['edit']) OR isset($_GET['add'])){ ?>
    <form id="form" action="/admin-panel/menu/ajax.php" method="post">
    <div class="row " >
      <div class="col-sm-6">
        <div class="card">
          <div class="card-header"> Collection Head Details</div>
          <div class="card-body"> 
            <div class="form-group">
                <label>SR.No</label>
                <input type="text" class="form-control form-control-sm" style="text-align:right;" name="sr_no" value="<?php if(isset($_GET['edit'])) { echo $data['sr_no'];}?>" >
            </div>  
            <div class="form-group">
                <label>Description On Bill</label>
                <textarea type="text" class="form-control form-control-sm" name="description_on_bill"><?php if(isset($_GET['edit'])){ echo $data['description_on_bill'];} ?></textarea>
            </div>  
         
            <div class="form-group">
            <label>Account Ledger</label>
              <select class="form-control form-control-sm rounded-0 " name="account_ledger" id="account_ledger" 
              >
              <option value="">Select Ledger Name</option>
                <?php while($row = $admin->fetch($getActiveLedgerDetails)){ ?>
                  <option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) and $data['account_ledger']==$row['id']) { echo 'selected'; } ?>><?php echo $row['ledger_name']; ?>
                  <?php } ?>
              </select>
            </div> 
            <div class="form-group">
                  <label>Short Description On Ledger Tally</label>
                  <input type="text" class="form-control form-control-sm" name="short_descp_in_ledger_tally" value="<?php if(isset($_GET['edit'])){ echo $data['short_descp_in_ledger_tally'];} ?>">
            </div>  
          </div>
        </div>
      </div>
     
      <div class="col-sm-6">
        <div class="card">
          <div class="card-header"> Calculation Details</div>
          <div class="card-body">
          <div class="form-group">
                <label>Rate Calculation On</label>
            </div> 
            <div class="form-group">
              <input type="radio" id="sq_ft"  name="rate_calc_on" value="sq_ft" <?php if(isset($_GET['edit']) && $data['rate_calc_on']=='sq_ft'){ echo "checked";}  ?>>
              <label for="sq_ft">SQ.ft</label>
              <input type="radio" id="fix"  name="rate_calc_on" value="fix" <?php if(isset($_GET['edit']) && $data['rate_calc_on']=='fix'){ echo "checked";}  ?>>
              <label for="fix">Fix</label>
            </div> 
            <div class="form-group">
                <label>Amount</label>
                <input type="text" class="form-control form-control-sm" style="text-align:right" name="rate_calc_amount" value="<?php if(isset($_GET['edit'])) { echo $data['rate_calc_amount'];} else {echo '0.00';}?>">
            </div> 
            <div class="form-group">
                <label>Interest Calculation</label>
                <input type="radio" id="yes"  name="interest_calc" value="yes" <?php if(isset($_GET['edit']) && $data['interest_calc']=='yes'){ echo "checked";}  ?>>
                <label for="yes">Yes</label>
                <input type="radio" id="no"  name="interest_calc" value="no" <?php if(isset($_GET['edit']) && $data['interest_calc']=='no'){ echo "checked";}  ?>>
                <label for="no">No</label>
            </div> 
            <div class="form-group">
            <label>GST Calculation</label>
                <input type="radio" id="yes"  name="gst_calc" value="yes" <?php if(isset($_GET['edit']) && $data['gst_calc']=='yes'){ echo "checked";}  ?>>
                <label for="yes">Yes</label>
                <input type="radio" id="no"  name="gst_calc" value="no" <?php if(isset($_GET['edit']) && $data['gst_calc']=='no'){ echo "checked";}  ?>>
                <label for="no">No</label>
            </div> 
            <div class="form-group">
                <label>SAC Code</label>
                <input type="text" class="form-control form-control-sm"  name="sac_code" value="<?php if(isset($_GET['edit'])){ echo $data['sac_code'];}?>">
            </div> 
            <div class="form-group">
                <label>GST %</label>
                <input type="text" class="form-control form-control-sm" style="text-align:right" name="gst_per" value="<?php if(isset($_GET['edit'])){ echo $data['gst_per'];} else {echo '0.00';}?>">
            </div> 
            <div class="form-group">
                <label>Check Per Month GST Limit</label>
                <input type="radio" id="yes"  name="check_per_month_gst_limit" value="yes" <?php if(isset($_GET['edit']) && $data['check_per_month_gst_limit']=='yes'){ echo "checked";}  ?>>
                <label for="yes">Yes</label>
                <input type="radio" id="no"  name="check_per_month_gst_limit" value="no" <?php if(isset($_GET['edit']) && $data['check_per_month_gst_limit']=='no'){ echo "checked";}  ?>>
                <label for="no">No</label>
            </div>   
          </div>
        </div>
      </div>
    </div>
  
    <div class="row">
      <div class="col-sm-6">
        <input type="hidden" class="form-control" name="ajax" value="<?php echo $pageURL; ?>"/>
        <?php if(isset($_GET['edit'])){ ?>
            <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>
            <button type="submit" style="margin:2px;" name="update" value="update" id="update" class="btn btn-warning btn-sm btn-block"><i class="fas fa-save"></i> Update <?php echo $pageName; ?></button>
            <?php } else { ?>
            <button type="submit"  style="margin:2px;" name="register" id="register" class="btn btn-success  btn-sm btn-block"><i class="fas fa-save"></i> Add <?php echo $pageName; ?></button>
            <?php } ?>
      </div>
      <div class="col-sm-6">
          <a  style="margin:2px;" class="btn btn-danger  btn-sm btn-block" href="/admin-panel/index.php" id="clearall"><i class="fas fa-broom "></i>Close</a>
      </div>
    </div>
</form> 
    
  <?php } ?>
  </div>
</div>
        
  <div class="card">
        <div class="card-header"> <?php echo $pageName; ?> List</div>
        <div class="card-body">   
  <table id="example" class="row-border" style="width:100%">
        <thead>
            <tr>
                <th>Sr. No.</th>
                <th>Description On Bill</th>
                <th>Account Ledger</th> 
                <th>Rate Calculation On</th>
                <th>Amount</th>
                <th>SAC Code</th>
                <th>GST %</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        <?php $x=1; while($row = $admin->fetch($results)){  
                $account_ledger_name = $admin -> getUniqueLedgerNameById($row['account_ledger'])['ledger_name'];
          ?>
            <tr>
                <td><?php echo $x;?></td>
                <td><?php echo $row['description_on_bill'];  ?></td>
                <td><?php  echo $account_ledger_name;  ?></td>
                <td><?php echo $row['rate_calc_on'];  ?></td>
                <td><?php echo $row['rate_calc_amount'];  ?></td>
                <td><?php echo $row['sac_code'];  ?></td>
                <td><?php echo $row['gst_per'];  ?></td>
                <td><a href="/admin-panel/index.php?edit&id=<?php echo $row['id']; ?>" class="btn btn-sm btn-warning">Edit</a></td>
            </tr>
            <?php $x++; } ?>
        </tbody>
        </div>
      </div>    
       
    </table>
   
  
</div>
<script>
   $(function () {
 $('#form').validate({
   rules: {
     ignore: [],
     debug: false,
     unit_no : {
        required: true,
     },
     owner_name : {
        required: true,
     },
   },
   messages: {
     email: {
       required: "Please enter a email address",
       email: "Please enter a vaild email address"
     },
   },
   errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.fromerrorcheck').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    },
    submitHandler: function(form) {
      $('#update').hide();
      $('#register').hide();
      $('#clearall').hide();
    $.ajax({
        url: form.action,
        type: form.method,
        data: $(form).serialize(),
        success: function(response) {
          window.location.href = '/admin-panel/index.php?msg='+response;
        }            
    });
}
 });
});

$('.nav-item').removeClass('active');
$('#master-page').addClass('active');
$(document).ready(function() {
  
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
} );
     
</script>