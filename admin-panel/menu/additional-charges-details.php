
<?php
session_start();
  include '../include/admin-functions.php';
  $admin = new AdminFunctions();

$mainPageName='Master';
$mainPageURL='dashboard';
$pageName='Additional Charges Details';
$pageURL='additional-charges-details';
$tableName='additional_charges';
$results = $admin->query("SELECT * FROM ".PREFIX.$tableName." WHERE deleted_time=0 GROUP BY id DESC");

setcookie('page',$pageURL, time() + (86400 * 30), "/");
if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
  $data = $admin->getUniqueAdditionalChargesMasterById($id);
}

?>
<?php if(isset($_GET['registersuccess'])){ ?>
  <script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Success('<?php echo $pageName; ?> successfully Added');
       </script>
<?php } ?>


<?php if(isset($_GET['updatesuccess'])){ ?>

<script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Warning('<?php echo $pageName; ?> successfully updated');
       </script>
<?php } ?>


<?php if(isset($_GET['deletesuccess'])){ ?>
<script>
       Notiflix.Notify.Init({});
       Notiflix.Notify.Failure('<?php echo $pageName; ?> successfully deleted');
       </script>

<?php } ?>
<ul class="breadcrumb">
  <li><a href="javascript:void(0);" > <?php echo $mainPageName; ?></a></li>
  <li><a href="javascript:void(0);" data-page="<?php echo $pageURL; ?>"> <?php echo $pageName; ?></a></li>
</ul>


<div class="container-fluid contentsection " id="cardeffect">
<div class="row">
<div class="col-sm-12">
<a href="<?php echo '/admin-panel/index.php?add'?>" class="btn btn-sm btn-primary" style="float:right;margin-bottom:5px;"><i class="material-icons">add</i> add New <?php echo $pageName; ?></a>
</div></div>
  <div class="row addsection " >
    <div class="col-sm-12">
    <?php if(isset($_GET['edit']) OR isset($_GET['add'])){ ?>
    <form id="form" action="/admin-panel/menu/ajax.php" method="post">
      <div class="card" >
        <div class="card-header"> <?php echo $pageName; ?>
        </div>
        <div class="card-body">       
        <div class="row">
            <div class="col-sm-2"> 
                <div class="form-group">
                    <label>Display Order</label>
                    <input type="text" class="form-control form-control-sm"  name="display_order" value="<?php if(isset($_GET['edit'])) { echo $data['display_order'];}?>"> 
                </div>  
            </div>
            <div class="col-sm-2"> 
                <div class="form-group">
                    <label>Description</label>
                    <input type="text" class="form-control form-control-sm"  name="description" value="<?php if(isset($_GET['edit'])) { echo $data['description'];}?>">            
                </div>  
            </div>
            <div class="col-sm-2"> 
                <div class="form-group">
                    <label>GST Applicable</label>
                    <select class="form-control form-control-sm" name="gst_applicable">
                            <option value="1" <?php if(isset($_GET['edit']) and $data['gst_applicable']=='1') { echo 'selected'; } ?>>Yes</option>
                            <option value="2" <?php if(isset($_GET['edit']) and $data['gst_applicable']=='2') { echo 'selected'; } ?>>No</option>
                        </select>    
                </div> 
            </div>
        </div>
        </div>
        <div class="card-footer" align="center"> 
                    <div class="row">
                    <div class="col-sm-6">
                    <input type="hidden" class="form-control" name="ajax" value="<?php echo $pageURL; ?>"/>
                    <?php if(isset($_GET['edit'])){ ?>
                        <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>
                        <button type="submit" style="margin:2px;" name="update" value="update" id="update" class="btn btn-warning btn-sm btn-block"><i class="fas fa-save"></i> Update <?php echo $pageName; ?></button>
                        <?php } else { ?>
                        <button type="submit"  style="margin:2px;" name="register" id="register" class="btn btn-success  btn-sm btn-block"><i class="fas fa-save"></i> Add <?php echo $pageName; ?></button>
                        <?php } ?>
                    </div>
                    <div class="col-sm-6">
                        <a  style="margin:2px;" class="btn btn-danger  btn-sm btn-block" href="/admin-panel/index.php" id="clearall"><i class="fas fa-broom "></i>Close</a>
                    </div>
                </div>
                </div>
                
      </div>
      </form>
      <?php } ?>
    </div>
  </div>
  <br>
  <div class="card">
        <div class="card-header"> <?php echo $pageName; ?> List</div>
        <div class="card-body">   
  <table id="example" class="row-border" style="width:100%">
        <thead>
            <tr>
                <th>Sr. No.</th>
                <th>Display Order</th>
                <th>Description</th>
                <th>GST Applicable</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        <?php $x=1; while($row = $admin->fetch($results)){  ?>
            <tr>
                <td><?php echo $x;?></td>
                <td><?php echo $row['display_order'];?></td>
                <td><?php echo $row['description'];?></td>
                <td><?php if($row['gst_applicable']==1){ echo 'Yes'; }elseif($row['gst_applicable']==2){ echo 'No'; }?></td>
                <td><a href="/admin-panel/index.php?edit&id=<?php echo $row['id']; ?>" class="btn btn-sm btn-warning">Edit</a></td>
            </tr>
            <?php $x++; } ?>
        </tbody>
        </div>
      </div>    
       
    </table>
   
  
</div>
<script>
   $(function () {
 $('#form').validate({
   rules: {
     ignore: [],
     debug: false,
     display_order : {
        required: true,
     },
     description : {
        required: true,
     },
     gst_applicable : {
        required: true,
     },
   },
   messages: {
     email: {
       required: "Please enter a email address",
       email: "Please enter a vaild email address"
     },
   },
   errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.fromerrorcheck').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    },
    submitHandler: function(form) {
      $('#update').hide();
      $('#register').hide();
      $('#clearall').hide();
    $.ajax({
        url: form.action,
        type: form.method,
        data: $(form).serialize(),
        success: function(response) {
          window.location.href = '/admin-panel/index.php?msg='+response;
        }            
    });
}
 });
});

$('.nav-item').removeClass('active');
$('#master-page').addClass('active');
$(document).ready(function() {
  
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
} );
     
</script>