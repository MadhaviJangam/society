<?php
	include_once 'config.php';
	include_once 'database.php';
	include_once 'Email.class.php';
	include_once 'SaveImage.class.php';
	include_once 'CSRF.class.php';
	include_once "Pagination.class.php";
//	include_once 'HelperFunctions.class.php';

	/*
	 * AdminFunctions
	 * v1.0.0 - updated loginSession(), logoutSession(), adminLogin()
	 * v1.1.0 - integrated FCMNotification.class.php class for handling push notifications
	 */
	class AdminFunctions extends Database {
		private $userType = 'admin';

		/*===================== LOGIN BEGINS =====================*/
		function loginSession($userId, $userFirstName, $userLastName, $userType) {
			$_SESSION[SITE_NAME][$this->userType."UserId"] = $userId;
			$_SESSION[SITE_NAME][$this->userType."UserFirstName"] = $userFirstName;
			$_SESSION[SITE_NAME][$this->userType."UserLastName"] = $userLastName;
			$_SESSION[SITE_NAME][$this->userType."UserType"] = $this->userType;
		}
		function logoutSession() {
			if(isset($_SESSION[SITE_NAME])){
				if(isset($_SESSION[SITE_NAME][$this->userType."UserId"])){
					unset($_SESSION[SITE_NAME][$this->userType."UserId"]);
				}
				if(isset($_SESSION[SITE_NAME][$this->userType."UserFirstName"])){
					unset($_SESSION[SITE_NAME][$this->userType."UserFirstName"]);
				}
				if(isset($_SESSION[SITE_NAME][$this->userType."UserLastName"])){
					unset($_SESSION[SITE_NAME][$this->userType."UserLastName"]);
				}
				if(isset($_SESSION[SITE_NAME][$this->userType."UserType"])){
					unset($_SESSION[SITE_NAME][$this->userType."UserType"]);
				}
				return true;
			} else {
				return false;
			}
		}
		function adminLogin($data, $successURL, $failURL = "index.php?failed") {
			$username = $this->escape_string($this->strip_all($data['email']));
			$password = $this->escape_string($this->strip_all($data['password']));
			$query = "select * from ".PREFIX."admin where email='".$username."'";
			$result = $this->query($query);

			if($this->num_rows($result) == 1) { // only one unique user should be present in the system
				$row = $this->fetch($result);
				if(password_verify($password, $row['password'])) {
					$this->loginSession($row['id'], $row['fname'], $row['lname'], $this->userType);
					$this->close_connection();
					header("location: ".$successURL);
					exit;
				} else {
					$this->close_connection();
					header("location: ".$failURL);
					exit;
				}
			} else {
				$this->close_connection();
				header("location: ".$failURL);
				exit;
			}
		}
		
		function sessionExists(){
			if($this->isUserLoggedIn()){
				return $loggedInUserDetailsArr = $this->getLoggedInUserDetails();
				// return true; // DEPRECATED
			} else {
				return false;
			}
		}
		function isUserLoggedIn(){
			if( isset($_SESSION[SITE_NAME]) && 
				isset($_SESSION[SITE_NAME][$this->userType.'UserId']) && 
				isset($_SESSION[SITE_NAME][$this->userType.'UserType']) && 
				!empty($_SESSION[SITE_NAME][$this->userType.'UserId']) &&
				$_SESSION[SITE_NAME][$this->userType.'UserType']==$this->userType){
				return true;
			} else {
				return false;
			}
		}
		function getSystemUserType() {
			return $this->userType;
		}
		function getLoggedInUserDetails(){
			$loggedInID = $this->escape_string($this->strip_all($_SESSION[SITE_NAME][$this->userType.'UserId']));
			$loggedInUserDetailsArr = $this->getUniqueAdminById($loggedInID);
			return $loggedInUserDetailsArr;
		}
		function getUniqueAdminById($userId) {
			$userId = $this->escape_string($this->strip_all($userId));
			$query = "select * from ".PREFIX."admin where id='".$userId."'";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}

		/** * Function to get details of admin */
		function getFirstAdminDetails(){
			$query = "select fname, lname, email from ".PREFIX."admin where user_role = 'super' limit 0, 1";
			$sql = $this->fetch($this->query($query));
			return $sql;
		}
		/*===================== LOGIN ENDS =====================*/


		
		/*===================== EXTRA FUNCTIONS BEGINS =====================*/
		
		/** * Function to create permalink */
		function getValidatedPermalink($permalink){ // v2.0.0
			$permalink = trim($permalink, '()');
			$replace_keywords = array("-:-", "-:", ":-", " : ", " :", ": ", ":",
				"-@-", "-@", "@-", " @ ", " @", "@ ", "@", 
				"-.-", "-.", ".-", " . ", " .", ". ", ".", 
				"-\\-", "-\\", "\\-", " \\ ", " \\", "\\ ", "\\",
				"-/-", "-/", "/-", " / ", " /", "/ ", "/", 
				"-&-", "-&", "&-", " & ", " &", "& ", "&", 
				"-,-", "-,", ",-", " , ", " ,", ", ", ",", 
				" ",
				"---", "--", " - ", " -", "- ",
				"-#-", "-#", "#-", " # ", " #", "# ", "#",
				"-$-", "-$", "$-", " $ ", " $", "$ ", "$",
				"-%-", "-%", "%-", " % ", " %", "% ", "%",
				"-^-", "-^", "^-", " ^ ", " ^", "^ ", "^",
				"-*-", "-*", "*-", " * ", " *", "* ", "*",
				"-(-", "-(", "(-", " ( ", " (", "( ", "(",
				"-)-", "-)", ")-", " ) ", " )", ") ", ")",
				"-;-", "-;", ";-", " ; ", " ;", "; ", ";",
				"-'-", "-'", "'-", " ' ", " '", "' ", "'",
				"-?-", "-?", "?-", " ? ", " ?", "? ", "?",
				'-"-', '-"', '"-', ' " ', ' "', '" ', '"',
				"-!-", "-!", "!-", " ! ", " !", "! ", "!");
			$escapedPermalink = str_replace($replace_keywords, '-', $permalink); 
			return strtolower($escapedPermalink);
		}

		/** * Function to get value in yes/no */
		function getActiveLabel($isActive){
			if($isActive){
				return 'Yes';
			} else {
				return 'No';
			}
		}

		/** * Function to get image url */
		function getImageDir($imageFor){
			switch($imageFor){
				case "company-logo":
					return "../images/content/company-logo/"; // add / at end
					break;
				case "job-seeker-profile-photos":
					return "../images/content/job-seeker-profile-photos/"; // add / at end
					break;
				case "industry_type_icons":
					return "../images/content/industry_type_icons/"; // add / at end
					break;
				default:
					return false;
					break;
			}
		}

		/** * Function to get image url */
		function getImageUrl($imageFor, $fileName, $imageSuffix, $dirPrefix = ""){
			$fileDir = $this->getImageDir($imageFor, $dirPrefix);
			if($fileDir === false){ // custom directory not found, error!
				$fileDir = "../images/"; // add / at end
				$defaultImageUrl = $fileDir."default.jpg";
				return $defaultImageUrl;
			} else { // process custom directory
				$defaultImageUrl = $fileDir."default.jpg";
				if(empty($fileName)){
					return $defaultImageUrl;
				} else {
					$image_name = strtolower(pathinfo($fileName, PATHINFO_FILENAME));
					$image_ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
					if(!empty($imageSuffix)){
						$imageUrl = $fileDir.$image_name."_".$imageSuffix.".".$image_ext;
					} else {
						$imageUrl = $fileDir.$image_name.".".$image_ext;
					}
					if(file_exists($imageUrl)){
						return $imageUrl;
					} else {
						return $defaultImageUrl;
					}
				}
			}
		}

		/** * Function to delete/unlink image file */
		function unlinkImage($imageFor, $fileName, $imageSuffix, $dirPrefix = ""){
			$fileDir = $this->getImageDir($imageFor, $dirPrefix);
			if($fileDir === false){ // custom directory not found, error!
				return false;
			} else { // process custom directory
				$defaultImageUrl = $fileDir."default.jpg";

				$imagePath = $this->getImageUrl($imageFor, $fileName, $imageSuffix, $dirPrefix);
				if($imagePath != $defaultImageUrl){
					$status = unlink($imagePath);
					return $status;
				} else {
					return false;
				}
			}
		}

		/** * Function to get uploaded documents url */
		function getDocDir($docFor){
			switch($docFor){
				case "address-proofs":
					return "../uploads/address-proofs/"; // add / at end
					break;
				case "company-profiles":
					return "../uploads/company-profiles/"; // add / at end
					break;
				case "resumes":
					return "../uploads/resumes/"; // add / at end
					break;
				case "educational-docs":
					return "../uploads/educational-docs/"; // add / at end
					break;
				default:
					return false;
					break;
			}
		}

		/** * Function to delete/unlink doc file */
		function unlinkDoc($docFor, $fileName){
			$fileDir = $this->getDocDir($docFor);
			if($fileDir === false){ // custom directory not found, error!
				return false;
			} else { // process custom directory
				
				$docPath = $fileDir."/".$fileName;
				return unlink($docPath);
			}
		}

		/** * Function to get remaining time/ elapsed time */
		function formatTimeRemainingInText($dateTime, $isComplete = false){
			if($isComplete){
				return "<strong>Complete!</strong>";
			} else if(!empty($dateTime)){
				$timestampDiff = strtotime($dateTime) - time();
				if($timestampDiff <=0 ){ // over due
					$then = new DateTime($dateTime);
					$now = new DateTime();
					$sinceThen = $now->diff($then);

					if($sinceThen->y > 0){
						return '<strong class="text-danger">'.$sinceThen->y." year(s) over due</strong>";
					}
					if($sinceThen->m > 0){
						return '<strong class="text-danger">'.$sinceThen->m." month(s) over due</strong>";
					}
					if($sinceThen->d > 0){
						return '<strong class="text-danger">'.$sinceThen->d." day(s) over due</strong>";
					}
					if($sinceThen->h > 0){
						return '<strong class="text-danger">'.$sinceThen->h." hour(s) over due</strong>";
					}
					if($sinceThen->i > 0){
						return '<strong class="text-danger">'.$sinceThen->i." minutes(s) over due</strong>";
					}
				} else { // time remaining
					$then = new DateTime($dateTime);
					$now = new DateTime();
					$sinceThen = $now->diff($then);

					if($sinceThen->y > 0){
						return $sinceThen->y." year(s) left";
					}
					if($sinceThen->m > 0){
						return $sinceThen->m." month(s) left";
					}
					if($sinceThen->d > 0){
						return $sinceThen->d." day(s) left";
					}
					if($sinceThen->h > 0){
						return '<strong class="text-danger">'.$sinceThen->h."</strong> hour(s) remaining";
					}
					if($sinceThen->i > 0){
						return '<strong class="text-danger">'.$sinceThen->i."</strong> minutes(s) remaining";
					}
				}

			} else {
				return "-";
			}
		}

		/** * Function to format date and time */
		function returnFormatTimeRemainingArray($dateTime, $isComplete = false){
			$resultArray = array();
			$resultArray['year'] = "0";
			$resultArray['month'] = "0";
			$resultArray['day'] = "0";
			$resultArray['hour'] = "0";
			$resultArray['minute'] = "0";
			$resultArray['second'] = "0";

			if($isComplete){
				$resultArray['overDue'] = false; // +
				return $resultArray;
			} else if(!empty($dateTime)){
				$then = new DateTime($dateTime);
				$now = new DateTime();
				$sinceThen = $now->diff($then);
				$resultArray['year'] = $sinceThen->y;
				$resultArray['month'] = $sinceThen->m;
				$resultArray['day'] = $sinceThen->d;
				$resultArray['hour'] = $sinceThen->h;
				$resultArray['minute'] = $sinceThen->i;
				$resultArray['second'] = $sinceThen->s;

				$timestampDiff = strtotime($dateTime) - time();
				if($timestampDiff <=0 ){ // over due
					$resultArray['overDue'] = true; // -
				} else { // time remaining
					$resultArray['overDue'] = false; // +
				}
				return $resultArray;
			} else {
				$resultArray['overDue'] = false; // +
				return $resultArray;
			}
		}

		/** * Function to format date and time */
		function formatDateTime($dateTime, $defaultFormat = "d M, Y h:i a T"){
			if(empty($dateTime)){
				return "-";
			} else {
				return date($defaultFormat, strtotime($dateTime));
			}
		}

		/** * Function to format date */
		function formatDate($dateTime, $defaultFormat = "d M, Y T"){
			if(empty($dateTime)){
				return "-";
			} else {
				return date($defaultFormat, strtotime($dateTime));
			}
		}

		/** * Function to format time */
		function formatTime($dateTime, $defaultFormat = "h:i a T"){
			if(empty($dateTime)){
				return "-";
			} else {
				return date($defaultFormat, strtotime($dateTime));
			}
		}

		/** * Function to limit text of description */
		function limitDescText($content, $charLength){
			if(strlen($content) > $charLength){
				return substr($content, 0, $charLength).'...';
			} else {
				return $content;
			}
		}

		/** * Function to format number in amount */
		function formatAmount($amount){
			$amount = (float) $amount;
			return number_format($amount,  2, '.', ',');
		}

		/** * Function to format number as text () */
		function formatNumberAsText($number){
			$numberLength = strlen($number);

			if($numberLength > 3){
				$number = (float) $number;
				$multiplier = 1;
				$suffix = "";
				switch($numberLength){
					case 4:
					case 5:
					case 6:
						$multiplier = 1000;
						$suffix = "K";
						break;
					case 7:
					case 8:
					case 9:
						$multiplier = 1000000;
						$suffix = "M";
						break;
					case 10:
					case 11:
					case 12:
						$multiplier = 1000000000;
						$suffix = "B";
						break;
				}
				$number = $number / $multiplier;
				$number = number_format($number,  1, '.', '');
				$number = $number.$suffix;
			}
			return $number;
		}

		/** * Function to format number as text () */
		function formatNumberAsIndianText($number){
			$numberLength = strlen($number);

			if($numberLength >= 3){
				$number = (float) $number;
				$multiplier = 1;
				$suffix = "";
				switch($numberLength){
					case 3:
					case 4:
					case 5:
						$multiplier = 1000;
						$suffix = "K";
						break;
					case 6:
					case 7:
						$multiplier = 100000;
						$suffix = " L";
						break;
					case 8:
					case 9:
					case 10:
					case 11:
					case 12:
						$multiplier = 10000000;
						$suffix = " Cr";
						break;
				}
				$number = $number / $multiplier;
				$number = number_format($number, 2, '.', ',');
				$number = $number.$suffix;
			}
			return $number;
		}

		/** * Function to validate numbers */
		function isNumericValue($value){

			return is_numeric($value);
		}

		/** * Function to validate percentage value */
		function isPercentValue($value){

			return ($value >=0 && $value <= 100);
		}

		/** * Function to check whether user has certain permissions as per given */
		function checkUserPermissions($permission,$loggedInUserDetailsArr) {
			$userPermissionsArray = explode(',',$loggedInUserDetailsArr['permissions']);
			if(!in_array($permission,$userPermissionsArray) and $loggedInUserDetailsArr['user_role']!='super') {
				header("location: index.php");
				exit;
			}
		}

		/** * Function to generate random unique number for particular column of particular table */
		function generate_id($prefix, $randomNo, $tableName, $columnName){
			$chkprofile=$this->query("select ".$columnName." from ".PREFIX.$tableName." where ".$columnName." = '".$prefix.$randomNo."'");
			if($this->num_rows($chkprofile)>0){
				$randomNo = str_shuffle('1234567890123456789012345678901234567890');
				$randomNo = substr($randomNo,0,8);
				$this->generate_id($prefix, $randomNo, $tableName, $columnName);
			}else{
				return  $prefix.$randomNo;
			}
		}

		/** * Function to get title of youtube video by its video id */
		function get_youtube_title($ref) {
	      	$json = file_get_contents('http://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=' . $ref . '&format=json'); //get JSON video details
	      	$details = json_decode($json, true); //parse the JSON into an array
	      	return $details['title']; //return the video title
	    }

		/** * Function to get ordinal with number */
	    function ordinal($number) {
		    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
		    if ((($number % 100) >= 11) && (($number%100) <= 13))
		        return $number. 'th';
		    else
		        return $number. $ends[$number % 10];
		}

		/** * Function to get months by selected option */
		function getAllMonths(){
			return array("1" => "January", "2" => "February", "3" => "March", "4" => "April", "5" => "May", "6" => "June", "7" => "July", "8" => "August", "9" => "September", "10" => "October", "11" => "November", "12" => "December");
		}
		/*===================== EXTRA FUNCTIONS ENDS =====================*/



		/*===================== SOCIETY MODULE BEGINS =====================*/

		function addSocietyMaster($data,$userId){

			$company_id = $this->escape_string($this->strip_all($data['company_id']));
			$society_name = $this->escape_string($this->strip_all($data['society_name']));
			$short_name = $this->escape_string($this->strip_all($data['short_name']));
			$registration_no = $this->escape_string($this->strip_all($data['registration_no']));
			$pan_no = $this->escape_string($this->strip_all($data['pan_no']));
			$tan_no = $this->escape_string($this->strip_all($data['tan_no']));
			$gst_no = $this->escape_string($this->strip_all($data['gst_no']));
			$societyCount = $this->fetch($this->query("SELECT COUNT(id) x FROM ".PREFIX."society_master"))['x'];
			$database = $this->fetch($this->query("SELECT * FROM ".PREFIX."admin WHERE id='".$userId."'"));
			$database_name = '';
			if($societyCount>=0 && $societyCount<5){	$database_name=$database['database1']; }
			if($societyCount>=5 && $societyCount<10){	$database_name=$database['database2']; }
			if($societyCount>=15 && $societyCount<20){	$database_name=$database['database4']; }
			if($societyCount>=20 && $societyCount<25){	$database_name=$database['database5']; }
			if($societyCount>=25 && $societyCount<30){	$database_name=$database['database6']; }
			if($societyCount>=30 && $societyCount<35){	$database_name=$database['database7']; }
			if($societyCount>=35 && $societyCount<40){	$database_name=$database['database8']; }
			if($societyCount>=40 && $societyCount<45){	$database_name=$database['database9']; }
			if($societyCount>=45 && $societyCount<50){	$database_name=$database['database10']; }
			if($database_name!=''){
				$query = $this->query("INSERT INTO ".PREFIX."society_master (company_id, society_name, short_name, registration_no, pan_no, tan_no, gst_no, created_by, created_time) values ('".$userId."', '".$society_name."', '".$short_name."', '".$registration_no."', '".$pan_no."', '".$tan_no."', '".$gst_no."','".$userId."', '".CURRENTDATETIME."')");
				$last_id = $this->last_insert_id();
				$query = $this->query("INSERT INTO ".PREFIX."database_mgt (company_id, society_id, database_name) values ('".$userId."', '".$last_id."', '".$database_name."')");   
			}
			 return $query;
		}

		function getUniqueSocietyMasterById($society_id){
			$query = $this->fetch($this->query("SELECT * FROM ".PREFIX."society_master WHERE id='".$society_id."' "));
			return $query;
		}
		function updateSociety($data,$company_id,$society_id,$created_by){
			$id = $this->escape_string($this->strip_all($data['id']));
			$society_name = $this->escape_string($this->strip_all($data['society_name']));
			$society_short_name = $this->escape_string($this->strip_all($data['society_short_name']));
			$society_address = $this->escape_string($this->strip_all($data['society_address']));
			$registration_no = $this->escape_string($this->strip_all($data['registration_no']));
			$date_of_incorp = $this->escape_string($this->strip_all($data['date_of_incorp']));
			$pan_no = $this->escape_string($this->strip_all($data['pan_no']));
			$tan_no = $this->escape_string($this->strip_all($data['tan_no']));
			$telephone_no = $this->escape_string($this->strip_all($data['telephone_no']));
			$email_id = $this->escape_string($this->strip_all($data['email_id']));
			$gst_no = $this->escape_string($this->strip_all($data['gst_no']));
			$gst_limit_per_month = $this->escape_string($this->strip_all($data['gst_limit_per_month']));
			$gst_applicable = $this->escape_string($this->strip_all($data['gst_applicable']));
			$bank_name = $this->escape_string($this->strip_all($data['bank_name']));
			$branch_name = $this->escape_string($this->strip_all($data['branch_name']));
			$account_no = $this->escape_string($this->strip_all($data['account_no']));
			$ifsc_no = $this->escape_string($this->strip_all($data['ifsc_no']));
			$intrest_calc = $this->escape_string($this->strip_all($data['intrest_calc']));
			$intrest_per = $this->escape_string($this->strip_all($data['intrest_per']));
			$intrest_per_after_amount = $this->escape_string($this->strip_all($data['intrest_per_after_amount']));
			$fix_amount = $this->escape_string($this->strip_all($data['fix_amount']));
			$fix_amount_after_amount = $this->escape_string($this->strip_all($data['fix_amount_after_amount']));
			$bill_format = $this->escape_string($this->strip_all($data['bill_format']));
			$print_on_bill = $this->escape_string($this->strip_all($data['print_on_bill']));
			$billing_cycle = $this->escape_string($this->strip_all($data['billing_cycle']));
			$receipt_to_be_adjust = $this->escape_string($this->strip_all($data['receipt_to_be_adjust']));
			$rebate_calc = $this->escape_string($this->strip_all($data['rebate_calc']));
			$only_in_april = $this->escape_string($this->strip_all($data['only_in_april']));
			$zero_pending = $this->escape_string($this->strip_all($data['zero_pending']));
			$rebate_one_day = $this->escape_string($this->strip_all($data['rebate_one_day']));
			$rebate_one_day_per = $this->escape_string($this->strip_all($data['rebate_one_day_per']));
			$rebate_two_day = $this->escape_string($this->strip_all($data['rebate_two_day']));
			$rebate_two_per = $this->escape_string($this->strip_all($data['rebate_two_per']));
			$foot_note = $this->escape_string($this->strip_all($data['foot_note']));
			
			$query = $this->query("UPDATE ".PREFIX."society_master SET company_id='".$company_id."', society_id='".$society_id."', society_name='".$society_name."', society_short_name='".$society_short_name."', society_address='".$society_address."',parking_rate='".$parking_rate."',registration_no='".$registration_no."',date_of_incorp='".$date_of_incorp."',pan_no='".$pan_no."',tan_no='".$tan_no."',telephone_no='".$telephone_no."',email_id='".$email_id."',gst_no='".$gst_no."',gst_limit_per_month='".$gst_limit_per_month."',gst_applicable='".$gst_applicable."',bank_name='".$bank_name."',branch_name='".$branch_name."',account_no='".$account_no."',ifsc_no='".$ifsc_no."',intrest_calc='".$intrest_calc."',intrest_per='".$intrest_per."',intrest_per_after_amount='".$intrest_per_after_amount."',fix_amount='".$fix_amount."',fix_amount_after_amount='".$fix_amount_after_amount."',bill_format='".$bill_format."',print_on_bill='".$print_on_bill."',billing_cycle='".$billing_cycle."',receipt_to_be_adjust='".$receipt_to_be_adjust."',rebate_calc='".$rebate_calc."',only_in_april='".$only_in_april."',zero_pending='".$zero_pending."',rebate_one_day='".$rebate_one_day."',rebate_one_day_per='".$rebate_one_day_per."',rebate_two_day='".$rebate_two_day."',rebate_two_per='".$rebate_two_per."',foot_note='".$foot_note."', updated_by='".$created_by."', updated_time='".CURRENTMILLIS."' WHERE id='".$id."' ");
			return $query;
		}
		function getMainMenu($company_id,$society_id){
			$result = $this->query("SELECT * FROM ".PREFIX."main_menu_master WHERE company_id='".$company_id."' AND society_id='".$society_id."' ");
			return $result;
		}

		function getSubMenu($company_id,$society_id,$main_menu_id){
			$result = $this->query("SELECT * FROM ".PREFIX."submenu_master WHERE company_id='".$company_id."' AND society_id='".$society_id."' AND main_menu_id='".$main_menu_id."' ");
			return $result;
		}

		/*===================== SOCIETY MODULE ENDS =====================*/

		/*===================== Parking Rate MODULE START =====================*/

		function getUniqueParkingRateMasterById($id){
			$query = $this->fetch($this->query("SELECT * FROM ".PREFIX."parking_master WHERE id='".$id."' "));
			return $query;
		}

		function addParkingRateMaster($data,$company_id,$society_id,$created_by){
			$parking_use_by = $this->escape_string($this->strip_all($data['parking_use_by']));
			$category_of_vehicle = $this->escape_string($this->strip_all($data['category_of_vehicle']));
			$parking_rate = $this->escape_string($this->strip_all($data['parking_rate']));
			$query = $this->query("INSERT INTO ".PREFIX."parking_master (company_id, society_id, parking_use_by, category_of_vehicle,parking_rate, created_by, created_time) values ('".$company_id."', '".$society_id."', '".$parking_use_by."', '".$category_of_vehicle."', '".$parking_rate."','".$created_by."', '".CURRENTMILLIS."')");
			return $query;
		}

		function updateParkingRateMaster($data,$company_id,$society_id,$created_by){
			$id = $this->escape_string($this->strip_all($data['id']));
			$parking_use_by = $this->escape_string($this->strip_all($data['parking_use_by']));
			$category_of_vehicle = $this->escape_string($this->strip_all($data['category_of_vehicle']));
			$parking_rate = $this->escape_string($this->strip_all($data['parking_rate']));
			$query = $this->query("UPDATE ".PREFIX."parking_master SET company_id='".$company_id."', society_id='".$society_id."', parking_use_by='".$parking_use_by."', category_of_vehicle='".$category_of_vehicle."', parking_rate='".$parking_rate."', updated_by='".$created_by."', updated_time='".CURRENTMILLIS."' WHERE id='".$id."' ");
			return $query;
		}
		/*===================== Parking Rate MODULE ENDS =====================*/


		/*===================== Customer Master MODULE START =====================*/

		function getUniqueCustomerMasterById($id){
			$query = $this->fetch($this->query("SELECT * FROM ".PREFIX."customer_master WHERE id='".$id."' "));
			return $query;
		}

		function addCustomerMaster($data,$company_id,$society_id,$created_by){
			$customer_name = $this->escape_string($this->strip_all($data['customer_name']));
			$address = $this->escape_string($this->strip_all($data['address']));
			$pan_no = $this->escape_string($this->strip_all($data['pan_no']));
			$opening_balance = $this->escape_string($this->strip_all($data['opening_balance']));
			$opening_balance_date = $this->escape_string($this->strip_all($data['opening_balance_date']));
			$state_code = $this->escape_string($this->strip_all($data['state_code']));
			$sac_code = $this->escape_string($this->strip_all($data['sac_code']));
			$query = $this->query("INSERT INTO ".PREFIX."customer_master (company_id, society_id, customer_name, address,pan_no,state_code,sac_code,opening_balance,opening_balance_date, created_by, created_time) values ('".$company_id."', '".$society_id."', '".$customer_name."', '".$address."', '".$pan_no."','".$state_code."','".$sac_code."','".$opening_balance."','".$opening_balance_date."','".$created_by."', '".CURRENTMILLIS."')");
			return $query;
		}

		function updateCustomerMaster($data,$company_id,$society_id,$created_by){
			$id = $this->escape_string($this->strip_all($data['id']));
			$customer_name = $this->escape_string($this->strip_all($data['customer_name']));
			$address = $this->escape_string($this->strip_all($data['address']));
			$pan_no = $this->escape_string($this->strip_all($data['pan_no']));
			$opening_balance = $this->escape_string($this->strip_all($data['opening_balance']));
			$opening_balance_date = $this->escape_string($this->strip_all($data['opening_balance_date']));
			$state_code = $this->escape_string($this->strip_all($data['state_code']));
			$sac_code = $this->escape_string($this->strip_all($data['sac_code']));
			
			
			$query = $this->query("UPDATE ".PREFIX."customer_master SET company_id='".$company_id."', society_id='".$society_id."', customer_name='".$customer_name."', address='".$address."', pan_no='".$pan_no."',opening_balance='".$opening_balance."',opening_balance_date='".$opening_balance_date."',state_code='".$state_code."',sac_code='".$sac_code."', update_by='".$created_by."', update_time='".CURRENTMILLIS."' WHERE id='".$id."' ");
			return $query;
		}
		
		/*===================== Parking Rate MODULE ENDS =====================*/
		/*===================== Supplier Master MODULE START =====================*/

		function getUniqueSupplierMasterById($id){
			$query = $this->fetch($this->query("SELECT * FROM ".PREFIX."supplier_master WHERE id='".$id."' "));
			return $query;
		}

		function addSupplierMaster($data,$company_id,$society_id,$created_by){
			$supplier_name = $this->escape_string($this->strip_all($data['supplier_name']));
			$address = $this->escape_string($this->strip_all($data['address']));
			$pan_no = $this->escape_string($this->strip_all($data['pan_no']));
			$opening_balance = $this->escape_string($this->strip_all($data['opening_balance']));
			$opening_balance_date = $this->escape_string($this->strip_all($data['opening_balance_date']));
			$state_code = $this->escape_string($this->strip_all($data['state_code']));
			$sac_code = $this->escape_string($this->strip_all($data['sac_code']));
			$query = $this->query("INSERT INTO ".PREFIX."supplier_master (company_id, society_id, supplier_name, address,pan_no,state_code,sac_code,opening_balance,opening_balance_date, created_by, created_time) values ('".$company_id."', '".$society_id."', '".$supplier_name."', '".$address."', '".$pan_no."','".$state_code."','".$sac_code."','".$opening_balance."','".$opening_balance_date."','".$created_by."', '".CURRENTMILLIS."')");
			return $query;
		}

		function updateSupplierMaster($data,$company_id,$society_id,$created_by){
			$id = $this->escape_string($this->strip_all($data['id']));
			$supplier_name = $this->escape_string($this->strip_all($data['supplier_name']));
			$address = $this->escape_string($this->strip_all($data['address']));
			$pan_no = $this->escape_string($this->strip_all($data['pan_no']));
			$opening_balance = $this->escape_string($this->strip_all($data['opening_balance']));
			$opening_balance_date = $this->escape_string($this->strip_all($data['opening_balance_date']));
			$state_code = $this->escape_string($this->strip_all($data['state_code']));
			$sac_code = $this->escape_string($this->strip_all($data['sac_code']));
			
			
			$query = $this->query("UPDATE ".PREFIX."supplier_master SET company_id='".$company_id."', society_id='".$society_id."', supplier_name='".$supplier_name."', address='".$address."', pan_no='".$pan_no."',opening_balance='".$opening_balance."',opening_balance_date='".$opening_balance_date."',state_code='".$state_code."',sac_code='".$sac_code."', update_by='".$created_by."', update_time='".CURRENTMILLIS."' WHERE id='".$id."' ");
			return $query;
		}
		
		/*===================== Supplier Master MODULE ENDS =====================*/
		/*===================== Additional Charges MODULE START =====================*/

		function getUniqueAdditionalChargesMasterById($id){
			$query = $this->fetch($this->query("SELECT * FROM ".PREFIX."additional_charges WHERE id='".$id."' "));
			return $query;
		}

		function addAdditionalChargesMaster($data,$company_id,$society_id,$created_by){
			$display_order = $this->escape_string($this->strip_all($data['display_order']));
			$description = $this->escape_string($this->strip_all($data['description']));
			$gst_applicable = $this->escape_string($this->strip_all($data['gst_applicable']));
			$query = $this->query("INSERT INTO ".PREFIX."additional_charges (company_id, society_id, display_order, description,gst_applicable, created_by, created_time) values ('".$company_id."', '".$society_id."', '".$display_order."', '".$description."', '".$gst_applicable."','".$created_by."', '".CURRENTMILLIS."')");
			return $query;
		}

		function updateAdditionalChargesMaster($data,$company_id,$society_id,$created_by){
			$id = $this->escape_string($this->strip_all($data['id']));
			$display_order = $this->escape_string($this->strip_all($data['display_order']));
			$description = $this->escape_string($this->strip_all($data['description']));
			$gst_applicable = $this->escape_string($this->strip_all($data['gst_applicable']));
			$query = $this->query("UPDATE ".PREFIX."additional_charges SET company_id='".$company_id."', society_id='".$society_id."', display_order='".$display_order."', description='".$description."', gst_applicable='".$gst_applicable."', updated_by='".$created_by."', updated_time='".CURRENTMILLIS."' WHERE id='".$id."' ");
			return $query;
		}
		/*===================== Additional Charges MODULE ENDS =====================*/
		/*===================== Bank Master MODULE START =====================*/

		function getUniqueBankMasterById($id){
			$query = $this->fetch($this->query("SELECT * FROM ".PREFIX."bank_master WHERE id='".$id."' "));
			return $query;
		}

		function addBankMaster($data,$company_id,$society_id,$created_by){
			$bank_name = $this->escape_string($this->strip_all($data['bank_name']));
			$branch_name = $this->escape_string($this->strip_all($data['branch_name']));
			$ifsc_code = $this->escape_string($this->strip_all($data['ifsc_code']));
			$query = $this->query("INSERT INTO ".PREFIX."bank_master (company_id, society_id, bank_name, branch_name,ifsc_code, created_by, created_time) values ('".$company_id."', '".$society_id."', '".$bank_name."', '".$branch_name."', '".$ifsc_code."','".$created_by."', '".CURRENTMILLIS."')");
			return $query;
		}

		function updateBankMaster($data,$company_id,$society_id,$created_by){
			$id = $this->escape_string($this->strip_all($data['id']));
			$bank_name = $this->escape_string($this->strip_all($data['bank_name']));
			$branch_name = $this->escape_string($this->strip_all($data['branch_name']));
			$ifsc_code = $this->escape_string($this->strip_all($data['ifsc_code']));
			$query = $this->query("UPDATE ".PREFIX."bank_master SET company_id='".$company_id."', society_id='".$society_id."', bank_name='".$bank_name."', branch_name='".$branch_name."', ifsc_code='".$ifsc_code."', updated_by='".$created_by."', updated_time='".CURRENTMILLIS."' WHERE id='".$id."' ");
			return $query;
		}
		/*===================== Additional Charges MODULE ENDS =====================*/
		/*===================== Group Master MODULE START =====================*/

		function getUniqueGroupMasterById($id){
			$query = $this->fetch($this->query("SELECT * FROM ".PREFIX."group_master WHERE id='".$id."' "));
			return $query;
		}

		function addGroupMaster($data,$company_id,$society_id,$created_by){
			$group_name = $this->escape_string($this->strip_all($data['group_name']));
			$active = $this->escape_string($this->strip_all($data['active']));
			$query = $this->query("INSERT INTO ".PREFIX."group_master (company_id, society_id, group_name, active, created_by, created_time) values ('".$company_id."', '".$society_id."', '".$group_name."', '".$active."','".$created_by."', '".CURRENTMILLIS."')");
			return $query;
		}

		function updateGroupMaster($data,$company_id,$society_id,$created_by){
			$id = $this->escape_string($this->strip_all($data['id']));
			$group_name = $this->escape_string($this->strip_all($data['group_name']));
			$active = $this->escape_string($this->strip_all($data['active']));
			$query = $this->query("UPDATE ".PREFIX."group_master SET company_id='".$company_id."', society_id='".$society_id."', group_name='".$group_name."', active='".$active."', updated_by='".$created_by."', updated_time='".CURRENTMILLIS."' WHERE id='".$id."' ");
			return $query;
		}
		/*===================== Group Master MODULE ENDS =====================*/
		/*===================== Ledger Master MODULE START =====================*/

		function getUniqueLedgerMasterById($id){
			$query = $this->fetch($this->query("SELECT * FROM ".PREFIX."ledger_master WHERE id='".$id."' "));
			return $query;
		}
		function getActiveLedgerDetails() {
			$query = "select * from ".PREFIX."ledger_master where active='1' ";
			return $this->query($query);
		}
		function getUniqueLedgerNameById($id) {
			$query = "select * from ".PREFIX."ledger_master where id='".$id."' AND active='1' ";
			$sql = $this->query($query);
			return $this->fetch($sql);
			
		}
		function addLedgerMaster($data,$company_id,$society_id,$created_by){
			$ledger_name = $this->escape_string($this->strip_all($data['ledger_name']));
			$active = $this->escape_string($this->strip_all($data['active']));
			$head_type = $this->escape_string($this->strip_all($data['head_type']));
			$query = $this->query("INSERT INTO ".PREFIX."ledger_master (company_id, society_id, ledger_name,head_type, active, created_by, created_time) values ('".$company_id."', '".$society_id."', '".$ledger_name."','".$head_type."', '".$active."','".$created_by."', '".CURRENTMILLIS."')");
			return $query;
		}

		function updateLedgerMaster($data,$company_id,$society_id,$created_by){
			$id = $this->escape_string($this->strip_all($data['id']));
			$ledger_name = $this->escape_string($this->strip_all($data['ledger_name']));
			$head_type = $this->escape_string($this->strip_all($data['head_type']));
			$active = $this->escape_string($this->strip_all($data['active']));
			$query = $this->query("UPDATE ".PREFIX."ledger_master SET company_id='".$company_id."', society_id='".$society_id."', ledger_name='".$ledger_name."',head_type='".$head_type."', active='".$active."', updated_by='".$created_by."', updated_time='".CURRENTMILLIS."' WHERE id='".$id."' ");
			return $query;
		}
		/*===================== Ledger Master MODULE ENDS =====================*/
		/*===================== Member Master MODULE START =====================*/

		function getUniqueMemberMasterById($id){
			$query = $this->fetch($this->query("SELECT * FROM ".PREFIX."member_master WHERE id='".$id."' "));
			return $query;
		}

		function addMemberMaster($data,$company_id,$society_id,$created_by){
			$space_type = $this->escape_string($this->strip_all($data['space_type']));
			$unit_no = $this->escape_string($this->strip_all($data['unit_no']));
			$owner_name = $this->escape_string($this->strip_all($data['owner_name']));
			$admission_date = $this->escape_string($this->strip_all($data['admission_date']));
			$sq_ft_area = $this->escape_string($this->strip_all($data['sq_ft_area']));
			$email_id = $this->escape_string($this->strip_all($data['email_id']));
			$phone_no = $this->escape_string($this->strip_all($data['phone_no']));
			$gst_no = $this->escape_string($this->strip_all($data['gst_no']));
			$pan_no = $this->escape_string($this->strip_all($data['pan_no']));
			$parking_slot_no = $this->escape_string($this->strip_all($data['parking_slot_no']));
			$parking_charges = $this->escape_string($this->strip_all($data['parking_charges']));
			$lain_bank_branch = $this->escape_string($this->strip_all($data['lain_bank_branch']));
			$from_date = $this->escape_string($this->strip_all($data['from_date']));
			$from_to = $this->escape_string($this->strip_all($data['from_to']));
			$noc_details = $this->escape_string($this->strip_all($data['noc_details']));
			$tenent = $this->escape_string($this->strip_all($data['tenent']));
			//$credit_days = $this->escape_string($this->strip_all($data['credit_days']));
			$query = $this->query("INSERT INTO ".PREFIX."member_master (company_id, society_id, space_type,unit_no,owner_name, admission_date,sq_ft_area,email_id,phone_no,gst_no,pan_no,parking_slot_no,parking_charges,lain_bank_branch,from_date,from_to,noc_details,tenent,created_by, created_time) values ('".$company_id."', '".$society_id."', '".$space_type."','".$unit_no."', '".$owner_name."','".$admission_date."','".$sq_ft_area."','".$email_id."','".$phone_no."','".$gst_no."','".$pan_no."','".$parking_slot_no."','".$parking_charges."','".$lain_bank_branch."','".$from_date."','".$from_to."','".$noc_details."','".$tenent."','".$created_by."', '".CURRENTMILLIS."')");
			return $query;
		}

		function updateMemberMaster($data,$company_id,$society_id,$created_by){
			$id = $this->escape_string($this->strip_all($data['id']));
			$space_type = $this->escape_string($this->strip_all($data['space_type']));
			$unit_no = $this->escape_string($this->strip_all($data['unit_no']));
			$owner_name = $this->escape_string($this->strip_all($data['owner_name']));
			$admission_date = $this->escape_string($this->strip_all($data['admission_date']));
			$sq_ft_area = $this->escape_string($this->strip_all($data['sq_ft_area']));
			$email_id = $this->escape_string($this->strip_all($data['email_id']));
			$phone_no = $this->escape_string($this->strip_all($data['phone_no']));
			$gst_no = $this->escape_string($this->strip_all($data['gst_no']));
			$pan_no = $this->escape_string($this->strip_all($data['pan_no']));
			$parking_slot_no = $this->escape_string($this->strip_all($data['parking_slot_no']));
			$parking_charges = $this->escape_string($this->strip_all($data['parking_charges']));
			$lain_bank_branch = $this->escape_string($this->strip_all($data['lain_bank_branch']));
			$from_date = $this->escape_string($this->strip_all($data['from_date']));
			$from_to = $this->escape_string($this->strip_all($data['from_to']));
			$noc_details = $this->escape_string($this->strip_all($data['noc_details']));
			$tenent = $this->escape_string($this->strip_all($data['tenent']));
			//$credit_days = $this->escape_string($this->strip_all($data['credit_days']));
			$query = $this->query("UPDATE ".PREFIX."member_master SET company_id='".$company_id."', society_id='".$society_id."', space_type='".$space_type."',unit_no='".$unit_no."', owner_name='".$owner_name."', admission_date='".$admission_date."',sq_ft_area='".$sq_ft_area."',email_id='".$email_id."',phone_no='".$phone_no."',gst_no='".$gst_no."',pan_no='".$pan_no."',parking_slot_no='".$parking_slot_no."',parking_charges='".$parking_charges."',lain_bank_branch='".$lain_bank_branch."',from_date='".$from_date."',from_to='".$from_to."',noc_details='".$noc_details."',tenent='".$tenent."',updated_by='".$created_by."', updated_time='".CURRENTMILLIS."' WHERE id='".$id."' ");
			return $query;
		}
		/*===================== Member Master MODULE ENDS =====================*/
		/*===================== Collection Head MODULE START =====================*/

		function getUniqueCollectionHeadById($id){
			$query = $this->fetch($this->query("SELECT * FROM ".PREFIX."collection_head WHERE id='".$id."' "));
			return $query;
		}

		function addCollectionHead($data,$company_id,$society_id,$created_by){
			$sr_no = $this->escape_string($this->strip_all($data['sr_no']));
			$description_on_bill = $this->escape_string($this->strip_all($data['description_on_bill']));
			$account_ledger = $this->escape_string($this->strip_all($data['account_ledger']));
			$short_descp_in_ledger_tally = $this->escape_string($this->strip_all($data['short_descp_in_ledger_tally']));
			$rate_calc_on = $this->escape_string($this->strip_all($data['rate_calc_on']));
			$rate_calc_amount = $this->escape_string($this->strip_all($data['rate_calc_amount']));
			$interest_calc = $this->escape_string($this->strip_all($data['interest_calc']));
			$gst_calc = $this->escape_string($this->strip_all($data['gst_calc']));
			$sac_code = $this->escape_string($this->strip_all($data['sac_code']));
			$gst_per = $this->escape_string($this->strip_all($data['gst_per']));
			$check_per_month_gst_limit = $this->escape_string($this->strip_all($data['check_per_month_gst_limit']));
			$query = $this->query("INSERT INTO ".PREFIX."collection_head (company_id, society_id, sr_no,description_on_bill,account_ledger, short_descp_in_ledger_tally,rate_calc_on,rate_calc_amount,interest_calc,gst_calc,sac_code,gst_per,check_per_month_gst_limit,created_by, created_time) values ('".$company_id."', '".$society_id."', '".$sr_no."','".$description_on_bill."', '".$account_ledger."','".$short_descp_in_ledger_tally."','".$rate_calc_on."','".$rate_calc_amount."','".$interest_calc."','".$gst_calc."','".$sac_code."','".$gst_per."','".$check_per_month_gst_limit."','".$created_by."', '".CURRENTMILLIS."')");
			return $query;
		}

		function updateCollectionHead($data,$company_id,$society_id,$created_by){
			$id = $this->escape_string($this->strip_all($data['id']));
			$sr_no = $this->escape_string($this->strip_all($data['sr_no']));
			$description_on_bill = $this->escape_string($this->strip_all($data['description_on_bill']));
			$account_ledger = $this->escape_string($this->strip_all($data['account_ledger']));
			$short_descp_in_ledger_tally = $this->escape_string($this->strip_all($data['short_descp_in_ledger_tally']));
			$rate_calc_on = $this->escape_string($this->strip_all($data['rate_calc_on']));
			$rate_calc_amount = $this->escape_string($this->strip_all($data['rate_calc_amount']));
			$interest_calc = $this->escape_string($this->strip_all($data['interest_calc']));
			$gst_calc = $this->escape_string($this->strip_all($data['gst_calc']));
			$sac_code = $this->escape_string($this->strip_all($data['sac_code']));
			$gst_per = $this->escape_string($this->strip_all($data['gst_per']));
			$check_per_month_gst_limit = $this->escape_string($this->strip_all($data['check_per_month_gst_limit']));
			$query = $this->query("UPDATE ".PREFIX."collection_head SET company_id='".$company_id."', society_id='".$society_id."', sr_no='".$sr_no."',description_on_bill='".$description_on_bill."', account_ledger='".$account_ledger."', short_descp_in_ledger_tally='".$short_descp_in_ledger_tally."',rate_calc_on='".$rate_calc_on."',rate_calc_amount='".$rate_calc_amount."',interest_calc='".$interest_calc."',gst_calc='".$gst_calc."',sac_code='".$sac_code."',gst_per='".$gst_per."',check_per_month_gst_limit='".$check_per_month_gst_limit."',updated_by='".$created_by."', updated_time='".CURRENTMILLIS."' WHERE id='".$id."' ");
			return $query;
		}
		/*===================== Collection Heads MODULE ENDS =====================*/
		
		// js-resume writing  ended
	}
?>