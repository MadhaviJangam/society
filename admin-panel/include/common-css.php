<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"><!-- For base grid system -->
<link href="css/londinium-theme.min.css" rel="stylesheet" type="text/css"><!-- For base main stylesheet -->
<link href="css/styles.min.css" rel="stylesheet" type="text/css"><!-- For overall website css stylesheet -->
<link href="css/icons.min.css" rel="stylesheet" type="text/css"><!-- For Icons used in admin panel -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css"><!-- For google font family used for website -->