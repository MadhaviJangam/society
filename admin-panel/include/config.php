<?php
	/*
	 * CONFIG
	 * - v1 - 
	 * - v2 - updated BASE CONFIG, error_reporting based on PROJECTSTATUS
	 * - v3 - added staging option
	 * - v3.1 - BUGFIX in staging option
	 */

	/* DEVELOPMENT CONFIG */
		DEFINE('PROJECTSTATUS','LIVE');
		//DEFINE('PROJECTSTATUS','DEMO');
		// DEFINE('PROJECTSTATUS','STAGING');
		 // DEFINE('PROJECTSTATUS','DEV');
	/* DEVELOPMENT CONFIG */

	/* TIMEZONE CONFIG */
	$timezone = "Asia/Kolkata";
	if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
	/* TIMEZONE CONFIG */
		
	/* SMS CONFIG */
		DEFINE('UNAME','');
		DEFINE('PASS','');
		DEFINE('SENDERID','');
	/* SMS CONFIG */
	
	if(PROJECTSTATUS == "LIVE"){
		//error_reporting(0);
		error_reporting(E_ALL);

		DEFINE('BASE_URL','http://society.local/');
		//DEFINE('ADMIN_EMAIL','info@jobringer.com');

		/* PAYMENT GATEWAY CONFIG - RAZORPAY */
	//		DEFINE('RAZORPAY_API_KEY','rzp_test_NcgmZkpI1odSfw');
		/* PAYMENT GATEWAY CONFIG */

	} else if(PROJECTSTATUS=="DEMO") {
		error_reporting(E_ALL);
		DEFINE('BASE_URL','http://www.jobringer.com/demo');
		DEFINE('ADMIN_EMAIL','info@jobringer.com');
		ini_set('display_errors', 'On');
		error_reporting(E_ALL);
		/* PAYMENT GATEWAY CONFIG - RAZORPAY */
			DEFINE('RAZORPAY_API_KEY','rzp_test_NcgmZkpI1odSfw');
		/* PAYMENT GATEWAY CONFIG */

		DEFINE('CAPTCHAV2SITEKEY','6LfzVpkUAAAAAEqwbz-UgLPazSP1kf_waAF9puxK');
		DEFINE('CAPTCHAV2SECRETKEY','6Lesz5kUAAAAALfBn9ChOXOVSf-c_X3zNDf3tDLE');

	} else if(PROJECTSTATUS=="STAGING"){
		error_reporting(E_ALL);
		DEFINE('BASE_URL','http://shareittofriends.com/demo/hindco-php');
		DEFINE('ADMIN_EMAIL','noreply@shareittofriends.com');

		/* PAYMENT GATEWAY CONFIG - RAZORPAY */
			DEFINE('RAZORPAY_API_KEY','rzp_test_NcgmZkpI1odSfw');
		/* PAYMENT GATEWAY CONFIG */

	} else { // DEFAULT TO DEV
		// ini_set('display_errors', "On");
		error_reporting(E_ALL);
		ini_set("log_errors", 1);
		ini_set("error_log", "error.log");
		// error_log( "Hello, errors!" );
		//DEFINE('BASE_URL','http://localhost/websites/hindco');
		DEFINE('BASE_URL','http://13.235.124.156/demo');
		DEFINE('ADMIN_EMAIL','info@hindco.com');

		/* PAYMENT GATEWAY CONFIG - RAZORPAY */
			DEFINE('RAZORPAY_API_KEY','rzp_test_NcgmZkpI1odSfw');
		/* PAYMENT GATEWAY CONFIG */

		/* GOOGLE CAPTCHA V2 CONFIG */
			
			//localhost
			/*DEFINE('CAPTCHAV2SITEKEY','6Le7P7QUAAAAAIMs9ZAJvVc2Fo67a8_JQFDElimz');
			DEFINE('CAPTCHAV2SECRETKEY','6Le7P7QUAAAAAFaP8xY8HXnkB1Q9t_G6sQ5aAnvN');*/

			//113
			DEFINE('CAPTCHAV2SITEKEY','6LfzVpkUAAAAAEqwbz-UgLPazSP1kf_waAF9puxK');
			DEFINE('CAPTCHAV2SECRETKEY','6Lesz5kUAAAAALfBn9ChOXOVSf-c_X3zNDf3tDLE');

		/* GOOGLE CAPTCHA V2 CONFIG */

	}

	/* BASE CONFIG */
		DEFINE('SITE_NAME','Unique Society');
		DEFINE('TITLE','Administrator Panel | '.SITE_NAME);
		DEFINE('PREFIX','so_');
		DEFINE('COPYRIGHT',date('Y'));
		DEFINE('LOGO', BASE_URL.'/images/logo.png');
		DEFINE('CURRENTMILLIS',round(microtime(true) * 1000));
		DEFINE('CURRENTDATETIME',date('Y-m-d H:i:s'));
	/* BASE CONFIG */
?>