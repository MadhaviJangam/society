<script type="text/javascript" src="js/jquery.min.js"></script><!-- Main jquery lib -->
<script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script><!-- For select dropdown -->
<script type="text/javascript" src="js/plugins/forms/select2.min.js"></script><!-- For datatables dropdown -->
<script type="text/javascript" src="js/plugins/forms/validate.min.js"></script><!-- For Form Validation -->
<script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script><!-- For Data tables -->
<script type="text/javascript" src="js/bootstrap.min.js"></script><!-- For base grid system -->
<script type="text/javascript" src="js/application.js"></script><!-- For -->
<script type="text/javascript" src="js/additional-methods.js"></script><!-- For Form Validation -->