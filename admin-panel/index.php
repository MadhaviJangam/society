<?php

session_start();

if(!isset($_SESSION['database_id'])){
  //header("location: /dashboard.php");
  echo 'Please Select Society';
  //exit();
}else{

  include 'include/admin-functions.php';
  $admin = new AdminFunctions();
 
  $mainMenuDetails = $admin->getMainMenu($_SESSION['company_id'],$_SESSION['society_id']);

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/jquery.dataTables.min.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/validate.min.js"></script>
  <script src="/js/jquery.dataTables.min.js"></script>
  <script src="/js/dataTables.buttons.min.js"></script>
  <script src="/js/jszip.min.js"></script>
  <script src="/js/pdfmake.min.js"></script>
  <script src="/js/vfs_fonts.js"></script>
  <script src="/js/buttons.html5.min.js"></script>
  <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script src="/js/notiflix-aio-1.5.0.min.js"></script>

</head>
<style>
.dataTables_wrapper {
    font-family: tahoma;
    font-size: 13px;

    position: relative;
    clear: both;
    *zoom: 1;
    zoom: 1;
}
.nav-link{
    color:black;font-size:15px; 
    width:100%;
}
.material-icons{
    display: inline-flex;
    vertical-align: top;
  
}
.active{
    border-bottom:2px solid #4834d4;
}
@media (min-width: 992px){
	.dropdown-menu .dropdown-toggle:after{
		border-top: .3em solid transparent;
	    border-right: 0;
	    border-bottom: .3em solid transparent;
	    border-left: .3em solid;
	}
	.dropdown-menu .dropdown-menu{
		margin-left:0; margin-right: 0;
	}
	.dropdown-menu li{
		position: relative;
	}
	.nav-item .submenu{ 
		display: none;
		position: absolute;
		left:100%; top:-7px;
	}
	.nav-item .submenu-left{ 
		right:100%; left:auto;
	}
	.dropdown-menu > li:hover{ background-color: #f1f1f1 }
	.dropdown-menu > li:hover > .submenu{
		display: block;
	}
}
 

.loader{
  margin: 0 0 2em;
  height: 100%;
  width: 100%;
  text-align: center;
  padding: 1em;
  margin: 0 auto 1em;
  display: inline-block;
  vertical-align: top;
}

/*
  Set the color of the icon
*/
svg path,
svg rect{
  fill: #FF6700;
}

.card{
border-top:4px solid #8e44ad;
border-radius:0px;
}

.card-header,.card-footer,.card-body {
    padding: 0.4rem 0.4rem;
    background:#fff;
    }
    .contentsection{
        margin-top:1%;
        margin-bottom:1%;
    }
.card-body > .form-group > label{
    font-size:10px;
    margin:1px;
}
.form-group > label{
    font-size:10px;
    margin:1px;
}
.form-group{
    margin:2px;
}
.form-control-sm {
    height: calc(1.5em + .5rem + 2px);
    padding: .2rem .2rem;
    border-radius: .0rem;
    border-bottom:1px solid #7f8c8d;
}

@media only screen and (max-width: 600px) {
 .navbardesktop{
   display:none;
 }
}

ul.breadcrumb {
  padding: 5px 7px;
  list-style: none;
  background-color: #eee;
  border-radius:0px;
}
ul.breadcrumb li {
  display: inline;
  font-size: 14px;
}
ul.breadcrumb li+li:before {
  padding: 4px;
  color: black;
  content: "/\00a0";
}
ul.breadcrumb li a {
  color: #0275d8;
  text-decoration: none;
}
ul.breadcrumb li a:hover {
  color: #01447e;
  text-decoration: underline;
}
.sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  border:1px solid #eee;
  background-color: #FFF;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 16px;
  color: #818181;
 
  display: block;
  transition: 0.3s;
}

.sidenav a:hover {
  color: #f1f1f1;
}

.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

#main {
  transition: margin-left .5s;
  padding: 16px;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
</style>
<body style="background:#f4f6fa;font-family: 'Noto Sans JP', sans-serif;">
<nav class="navbar navbar-expand-md navbar-light d-print-none " style="border-bottom:2px solid#eee;background:#FFF;padding:2px;">
  <a class="navbar-brand" href="#" style="color:black;font-size:14px;"><i class="material-icons">location_city</i> UNIQUE SOCIETY</a>
  <button class="navbar-toggler navbardesktop" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <button class="navbar-toggler" type="button"  onclick="openNav()" >
    <span class="navbar-toggler-icon"></span>
  </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">

    <ul class="navbar-nav ml-auto">
      <li><a class="nav-link active" href="#"> <i class="material-icons">settings</i> Profile Settings</a></li>
    </ul>
  </div>  
</nav>



<nav class="navbar navbar-expand-md navbar-light d-print-none navbardesktop" style="border-bottom:2px solid #eee;background:#FFF;padding-right:8%;padding-left:8%;padding-bottom:0px;padding-top:0px;">
<ul class="navbar-nav">
    <li class="nav-item active" id="home-page">
      <a class="nav-link"  href="javascript:void(0);" id="dashboard"><i class="material-icons">home</i><span>Home</span></a>
    </li>
    <?php
       while ($row = $admin->fetch($mainMenuDetails)) {
       $subMenuDetails = $admin-> getSubMenu($_SESSION['company_id'],$_SESSION['society_id'],$row['id']);
         ?>
  <li class="nav-item dropdown" id="master-page">
    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"><i class="material-icons"><?php echo $row['icon_name']; ?></i> <span><?php echo $row['main_manu_name']; ?></span></a>
    
    <ul class="dropdown-menu">
    <?php  while ($rows = $admin->fetch($subMenuDetails)) { ?>
	      <li><a class="dropdown-item" href="javascript:void(0);" data-page="<?php echo str_replace(' ', '-',strtolower($rows['sub_menu_name'])); ?>"> <?php echo $rows['sub_menu_name']; ?> </a></li>
    <?php } ?>
    </ul>
    </li>

   <?php } ?>  
  </ul>

  <ul class="navbar-nav ml-auto">
      <li><a class="nav-link active" href="#"><i class="material-icons">account_circle</i> <span>Sameer Bayani</span></a></li>
      <li><a class="nav-link" style="border-bottom:2px solid red;color:red;" href="logout.php"><i class="material-icons">exit_to_app</i> <span>Logout</span></a></li>
    </ul>
</nav>


<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="#">About</a>
  <a href="#">Services</a>
  <a href="#">Clients</a>
  <a href="#">Contact</a>
</div>

<div class="loader loader--style1" title="0">
  <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
   width="150px" height="150px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
  <path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946
    s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634
    c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"/>
  <path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0
    C22.32,8.481,24.301,9.057,26.013,10.047z">
    <animateTransform attributeType="xml"
      attributeName="transform"
      type="rotate"
      from="0 20 20"
      to="360 20 20"
      dur="0.8s"
      repeatCount="indefinite"/>
    </path>
  </svg>
</div>


<div id="main-ui">

</div>
<!-- 3  -->

<script>
$(document).on('click', '.dropdown-menu', function (e) {
  e.stopPropagation();
});

// make it as accordion for smaller screens
if ($(window).width() < 992) {
  $('.dropdown-menu a').click(function(e){
    e.preventDefault();
      if($(this).next('.submenu').length){
        $(this).next('.submenu').toggle();
      }
      $('.dropdown').on('hide.bs.dropdown', function () {
     $(this).find('.submenu').hide();
  })
  });
}

$('.dropdown-item').on('click', function () {
  let pagename = $(this).data('page');
  //if($("#" + pagename).length != 0) {
  $('#main-ui').html('');
  $('.loader').show();
  $.ajax({
    type: "GET",
    url: 'menu/' + pagename + '.php',
    success: function (res) {
      $('#main-ui').html(res);
      $('.loader').hide();
    }
  });
  //}
})


// $('.nav-link').on('click',function(){
//     let pagename=$(this).attr('id');
//     if($("#" + pagename).length != 0) {
//         $('#main-ui').html('');
//         $('.loader').show();
//         $.ajax({
// 		type: "GET",
// 		url: 'menu/'+pagename+'.php',
// 		success: function (res) {
//             $('#main-ui').html(res);
//             $('.loader').hide();
// 		}
//     });
// }
// })

let default_url ='';
if(readCookie('page')==null){
   default_url = 'dashboard';
}else{
   default_url = readCookie('page');
}

$.ajax({
		type: "GET",
		url: 'menu/'+default_url+'.php<?php if(isset($_GET['edit'])){ echo '?edit&id='.$_GET['id']; } if(isset($_GET['msg'])){ echo '?'.$_GET['msg']; } if(isset($_GET['add'])){ echo '?add'; } ?>',
		success: function (res) {
            $('#main-ui').html(res);
            $('.loader').hide();
		}
    });

    function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.body.style.backgroundColor = "white";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

</script>
</body>
</html>
<?php } ?>